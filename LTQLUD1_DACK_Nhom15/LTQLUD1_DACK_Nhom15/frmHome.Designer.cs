﻿namespace LTQLUD1_DACK_Nhom15
{
    partial class frmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHome));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lbTittle = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.userName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelQLSach = new System.Windows.Forms.Panel();
            this.tbcQuanLiSach = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnYeuCauTL = new System.Windows.Forms.Button();
            this.btnXemTheoLoai = new System.Windows.Forms.Button();
            this.cbxLoaiTaiLieu = new System.Windows.Forms.ComboBox();
            this.btnChinhSuaTL = new System.Windows.Forms.Button();
            this.btnHuyTL = new System.Windows.Forms.Button();
            this.btnLuuTL = new System.Windows.Forms.Button();
            this.btnXemAllTaiLieu = new System.Windows.Forms.Button();
            this.btnLapPhieuMuonTL = new System.Windows.Forms.Button();
            this.btnXoaTL = new System.Windows.Forms.Button();
            this.btnXemChiTietTL = new System.Windows.Forms.Button();
            this.dgvSearchTaiLieu = new System.Windows.Forms.DataGridView();
            this.btnSearchTaiLieu = new System.Windows.Forms.Button();
            this.lblTenTaiLieu = new System.Windows.Forms.Label();
            this.lblMaTaiLieu = new System.Windows.Forms.Label();
            this.rdTimTLNangCao = new System.Windows.Forms.RadioButton();
            this.rdTimTLCoBan = new System.Windows.Forms.RadioButton();
            this.txtSearchTaiLieu = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnThemTaiLieu = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMaTL = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSoLuongTL = new System.Windows.Forms.TextBox();
            this.txtLoaiTL = new System.Windows.Forms.TextBox();
            this.txtHienTrangTL = new System.Windows.Forms.TextBox();
            this.txtTenTL = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnNhanVien = new System.Windows.Forms.Button();
            this.btnDocGia = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnThongKe = new System.Windows.Forms.Button();
            this.btnDangNhap = new System.Windows.Forms.Button();
            this.tbcDocGia = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.cbxDinhDanh = new System.Windows.Forms.ComboBox();
            this.btnChinhSua = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.btnLuu = new System.Windows.Forms.Button();
            this.btnXemAllDocGia = new System.Windows.Forms.Button();
            this.btnLapPhieuCanhCao = new System.Windows.Forms.Button();
            this.btnLapPhieuTra = new System.Windows.Forms.Button();
            this.btnLapPhieMuon = new System.Windows.Forms.Button();
            this.btnXoaDocGia = new System.Windows.Forms.Button();
            this.btnXemChiTiet = new System.Windows.Forms.Button();
            this.dgvDGSearch = new System.Windows.Forms.DataGridView();
            this.btnSearchDocGia = new System.Windows.Forms.Button();
            this.lblHoTenSearch = new System.Windows.Forms.Label();
            this.lblDinhDanhSearch = new System.Windows.Forms.Label();
            this.lblMaDGSearch = new System.Windows.Forms.Label();
            this.rdHoTenSearch = new System.Windows.Forms.RadioButton();
            this.rdMaDinhDanhSearch = new System.Windows.Forms.RadioButton();
            this.rdMaDGSearch = new System.Windows.Forms.RadioButton();
            this.txtSearchDG = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tabThemdg = new System.Windows.Forms.TabPage();
            this.btnDangKyDocGia = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtMaDG = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblMSCBDangKy = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblMSSVDangKy = new System.Windows.Forms.Label();
            this.rdKhac = new System.Windows.Forms.RadioButton();
            this.rdCanBo = new System.Windows.Forms.RadioButton();
            this.rdSinhVien = new System.Windows.Forms.RadioButton();
            this.txtCMNDDG = new System.Windows.Forms.TextBox();
            this.txtMSCBDG = new System.Windows.Forms.TextBox();
            this.txtMSSVDG = new System.Windows.Forms.TextBox();
            this.txtEmailDG = new System.Windows.Forms.TextBox();
            this.txtSDTDG = new System.Windows.Forms.TextBox();
            this.txtDiaChiDG = new System.Windows.Forms.TextBox();
            this.txtNgaySinhDG = new System.Windows.Forms.TextBox();
            this.txtTenDG = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbUserName = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.panelDocGia = new System.Windows.Forms.Panel();
            this.panelQLNhanVien = new System.Windows.Forms.Panel();
            this.tbcQuanLiNV = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgvNhanVien = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.txtMatKhauNVCapNhat = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.rdAdminCapNhat = new System.Windows.Forms.RadioButton();
            this.rdThuThuCapNhat = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.btnCapNhatNhanVien = new System.Windows.Forms.Button();
            this.txtHoTenNVCapNhat = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTenDangNhapNVCapNhat = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCaTrucNVCapNhat = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMaNVCapNhap = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btnXoaNhanVien = new System.Windows.Forms.Button();
            this.txtMaNVXoa = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.pnThongKe = new System.Windows.Forms.Panel();
            this.pnthu = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pnLapPhieumuon = new System.Windows.Forms.Panel();
            this.btnLuuPhieuMuon = new System.Windows.Forms.Button();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.dtphantraphieumuon = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.txtMatailieumuon = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtSTTmuon = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.dtpNgayLapphieumuon = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.txtMadocgiamuon = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMaphieumuon = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.pnxemphieumuon = new System.Windows.Forms.Panel();
            this.pnNhapthongtintimkiemphieumuon = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.dgvxemphieumuon = new System.Windows.Forms.DataGridView();
            this.btnXmuon = new System.Windows.Forms.Button();
            this.btnSmuon = new System.Windows.Forms.Button();
            this.btnCNmuon = new System.Windows.Forms.Button();
            this.btnXPmuon = new System.Windows.Forms.Button();
            this.btnLPmuon = new System.Windows.Forms.Button();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.pnlapphieutra = new System.Windows.Forms.Panel();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.pnXemPhieuTra = new System.Windows.Forms.Panel();
            this.dgvXemPhieuTra = new System.Windows.Forms.DataGridView();
            this.btnXoaPhieuTra = new System.Windows.Forms.Button();
            this.btnSuaPhieuTra = new System.Windows.Forms.Button();
            this.btnCapNhatPhieuTra = new System.Windows.Forms.Button();
            this.btnXemPhieuTra = new System.Windows.Forms.Button();
            this.btnLpTra = new System.Windows.Forms.Button();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.pnLapPhieuNhacNho = new System.Windows.Forms.Panel();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.pnXemPhieuNhacNho = new System.Windows.Forms.Panel();
            this.dgvXemPhieuNhacNho = new System.Windows.Forms.DataGridView();
            this.btnXoaPhieuNhacNho = new System.Windows.Forms.Button();
            this.btnSuaPhieuNhacNho = new System.Windows.Forms.Button();
            this.btnCapNhatPhieuNhacNho = new System.Windows.Forms.Button();
            this.btnXemPNhacNho = new System.Windows.Forms.Button();
            this.btnLPNhacNho = new System.Windows.Forms.Button();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.pnLapPhieuPhat = new System.Windows.Forms.Panel();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label37 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.pnXemPhieuPhat = new System.Windows.Forms.Panel();
            this.dgvXemPhieuPhat = new System.Windows.Forms.DataGridView();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.btnXemPhieuPhat = new System.Windows.Forms.Button();
            this.btnLapPhieuPhat = new System.Windows.Forms.Button();
            this.lbTimkiemDG = new System.Windows.Forms.Label();
            this.lbTimKiemSach = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.panelTraCuu = new System.Windows.Forms.Panel();
            this.tabPhieuPhat = new System.Windows.Forms.TabPage();
            this.tabPhieuNhacNho = new System.Windows.Forms.TabPage();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.button38 = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.button47 = new System.Windows.Forms.Button();
            this.label52 = new System.Windows.Forms.Label();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.tabPhieuMuon = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabThemPhieuMuon = new System.Windows.Forms.TabPage();
            this.tabTimKiemPhieuMuon = new System.Windows.Forms.TabPage();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button17 = new System.Windows.Forms.Button();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label47 = new System.Windows.Forms.Label();
            this.button48 = new System.Windows.Forms.Button();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label55 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.label58 = new System.Windows.Forms.Label();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.label48 = new System.Windows.Forms.Label();
            this.button29 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.button28 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.tabPhieuTra = new System.Windows.Forms.TabPage();
            this.label49 = new System.Windows.Forms.Label();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.dateTimePicker7 = new System.Windows.Forms.DateTimePicker();
            this.label64 = new System.Windows.Forms.Label();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.button49 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.button56 = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelQLSach.SuspendLayout();
            this.tbcQuanLiSach.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearchTaiLieu)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tbcDocGia.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDGSearch)).BeginInit();
            this.tabThemdg.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panelDocGia.SuspendLayout();
            this.panelQLNhanVien.SuspendLayout();
            this.tbcQuanLiNV.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhanVien)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnThongKe.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pnLapPhieumuon.SuspendLayout();
            this.pnxemphieumuon.SuspendLayout();
            this.pnNhapthongtintimkiemphieumuon.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvxemphieumuon)).BeginInit();
            this.tabPage9.SuspendLayout();
            this.pnlapphieutra.SuspendLayout();
            this.pnXemPhieuTra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvXemPhieuTra)).BeginInit();
            this.tabPage10.SuspendLayout();
            this.pnLapPhieuNhacNho.SuspendLayout();
            this.pnXemPhieuNhacNho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvXemPhieuNhacNho)).BeginInit();
            this.tabPage11.SuspendLayout();
            this.pnLapPhieuPhat.SuspendLayout();
            this.pnXemPhieuPhat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvXemPhieuPhat)).BeginInit();
            this.panelTraCuu.SuspendLayout();
            this.tabPhieuPhat.SuspendLayout();
            this.tabPhieuNhacNho.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tabPhieuMuon.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabThemPhieuMuon.SuspendLayout();
            this.tabTimKiemPhieuMuon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPhieuTra.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.ForeColor = System.Drawing.Color.Transparent;
            this.groupBox1.Location = new System.Drawing.Point(1050, -1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(78, 32);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // button3
            // 
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(43, 33);
            this.button3.TabIndex = 2;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(39, -2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 35);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbTittle
            // 
            this.lbTittle.AutoSize = true;
            this.lbTittle.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lbTittle.Font = new System.Drawing.Font("Constantia", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTittle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(154)))));
            this.lbTittle.Location = new System.Drawing.Point(316, 25);
            this.lbTittle.Name = "lbTittle";
            this.lbTittle.Size = new System.Drawing.Size(385, 29);
            this.lbTittle.TabIndex = 1;
            this.lbTittle.Text = "PHẦM MỀM QUẢN LÝ THƯ VIỆN";
            this.lbTittle.Click += new System.EventHandler(this.lbTittle_Click);
            this.lbTittle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.lbTittle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.lbTittle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(865, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 45);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // userName
            // 
            this.userName.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.userName.Location = new System.Drawing.Point(933, 40);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(153, 31);
            this.userName.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Calibri", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(952, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 24);
            this.label1.TabIndex = 10;
            this.label1.Text = "Quyền - Admin ";
            // 
            // panelQLSach
            // 
            this.panelQLSach.Controls.Add(this.tbcQuanLiSach);
            this.panelQLSach.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panelQLSach.Location = new System.Drawing.Point(245, 64);
            this.panelQLSach.Name = "panelQLSach";
            this.panelQLSach.Size = new System.Drawing.Size(911, 449);
            this.panelQLSach.TabIndex = 11;
            // 
            // tbcQuanLiSach
            // 
            this.tbcQuanLiSach.Controls.Add(this.tabPage1);
            this.tbcQuanLiSach.Controls.Add(this.tabPage8);
            this.tbcQuanLiSach.ItemSize = new System.Drawing.Size(110, 25);
            this.tbcQuanLiSach.Location = new System.Drawing.Point(0, 0);
            this.tbcQuanLiSach.Name = "tbcQuanLiSach";
            this.tbcQuanLiSach.Padding = new System.Drawing.Point(12, 3);
            this.tbcQuanLiSach.SelectedIndex = 0;
            this.tbcQuanLiSach.Size = new System.Drawing.Size(911, 446);
            this.tbcQuanLiSach.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage1.Controls.Add(this.btnYeuCauTL);
            this.tabPage1.Controls.Add(this.btnXemTheoLoai);
            this.tabPage1.Controls.Add(this.cbxLoaiTaiLieu);
            this.tabPage1.Controls.Add(this.btnChinhSuaTL);
            this.tabPage1.Controls.Add(this.btnHuyTL);
            this.tabPage1.Controls.Add(this.btnLuuTL);
            this.tabPage1.Controls.Add(this.btnXemAllTaiLieu);
            this.tabPage1.Controls.Add(this.btnLapPhieuMuonTL);
            this.tabPage1.Controls.Add(this.btnXoaTL);
            this.tabPage1.Controls.Add(this.btnXemChiTietTL);
            this.tabPage1.Controls.Add(this.dgvSearchTaiLieu);
            this.tabPage1.Controls.Add(this.btnSearchTaiLieu);
            this.tabPage1.Controls.Add(this.lblTenTaiLieu);
            this.tabPage1.Controls.Add(this.lblMaTaiLieu);
            this.tabPage1.Controls.Add(this.rdTimTLNangCao);
            this.tabPage1.Controls.Add(this.rdTimTLCoBan);
            this.tabPage1.Controls.Add(this.txtSearchTaiLieu);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(903, 413);
            this.tabPage1.TabIndex = 6;
            this.tabPage1.Text = "Tìm KiếmTài Liệu";
            // 
            // btnYeuCauTL
            // 
            this.btnYeuCauTL.BackColor = System.Drawing.Color.Green;
            this.btnYeuCauTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnYeuCauTL.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnYeuCauTL.Location = new System.Drawing.Point(684, 349);
            this.btnYeuCauTL.Name = "btnYeuCauTL";
            this.btnYeuCauTL.Size = new System.Drawing.Size(176, 41);
            this.btnYeuCauTL.TabIndex = 24;
            this.btnYeuCauTL.Text = "Yêu Cầu Tài Liệu Mới";
            this.btnYeuCauTL.UseVisualStyleBackColor = false;
            this.btnYeuCauTL.Click += new System.EventHandler(this.btnYeuCauTL_Click);
            // 
            // btnXemTheoLoai
            // 
            this.btnXemTheoLoai.BackColor = System.Drawing.Color.Purple;
            this.btnXemTheoLoai.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnXemTheoLoai.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXemTheoLoai.Location = new System.Drawing.Point(612, 67);
            this.btnXemTheoLoai.Name = "btnXemTheoLoai";
            this.btnXemTheoLoai.Size = new System.Drawing.Size(129, 33);
            this.btnXemTheoLoai.TabIndex = 23;
            this.btnXemTheoLoai.Text = "Xem Theo Loại";
            this.btnXemTheoLoai.UseVisualStyleBackColor = false;
            this.btnXemTheoLoai.Click += new System.EventHandler(this.btnXemTheoLoai_Click);
            // 
            // cbxLoaiTaiLieu
            // 
            this.cbxLoaiTaiLieu.FormattingEnabled = true;
            this.cbxLoaiTaiLieu.Location = new System.Drawing.Point(758, 75);
            this.cbxLoaiTaiLieu.Name = "cbxLoaiTaiLieu";
            this.cbxLoaiTaiLieu.Size = new System.Drawing.Size(121, 21);
            this.cbxLoaiTaiLieu.TabIndex = 22;
            this.cbxLoaiTaiLieu.Text = "SÁCH";
            // 
            // btnChinhSuaTL
            // 
            this.btnChinhSuaTL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnChinhSuaTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnChinhSuaTL.ForeColor = System.Drawing.Color.White;
            this.btnChinhSuaTL.Location = new System.Drawing.Point(155, 345);
            this.btnChinhSuaTL.Name = "btnChinhSuaTL";
            this.btnChinhSuaTL.Size = new System.Drawing.Size(117, 43);
            this.btnChinhSuaTL.TabIndex = 21;
            this.btnChinhSuaTL.Text = "Chỉnh Sửa";
            this.btnChinhSuaTL.UseVisualStyleBackColor = false;
            this.btnChinhSuaTL.Click += new System.EventHandler(this.btnChinhSuaTL_Click);
            // 
            // btnHuyTL
            // 
            this.btnHuyTL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnHuyTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnHuyTL.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnHuyTL.Location = new System.Drawing.Point(617, 346);
            this.btnHuyTL.Name = "btnHuyTL";
            this.btnHuyTL.Size = new System.Drawing.Size(142, 44);
            this.btnHuyTL.TabIndex = 20;
            this.btnHuyTL.Text = "Hủy";
            this.btnHuyTL.UseVisualStyleBackColor = false;
            this.btnHuyTL.Click += new System.EventHandler(this.btnHuyTL_Click);
            // 
            // btnLuuTL
            // 
            this.btnLuuTL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnLuuTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnLuuTL.ForeColor = System.Drawing.Color.White;
            this.btnLuuTL.Location = new System.Drawing.Point(400, 347);
            this.btnLuuTL.Name = "btnLuuTL";
            this.btnLuuTL.Size = new System.Drawing.Size(117, 43);
            this.btnLuuTL.TabIndex = 19;
            this.btnLuuTL.Text = "Lưu";
            this.btnLuuTL.UseVisualStyleBackColor = false;
            this.btnLuuTL.Click += new System.EventHandler(this.btnLuuTL_Click);
            // 
            // btnXemAllTaiLieu
            // 
            this.btnXemAllTaiLieu.BackColor = System.Drawing.Color.Purple;
            this.btnXemAllTaiLieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnXemAllTaiLieu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXemAllTaiLieu.Location = new System.Drawing.Point(617, 20);
            this.btnXemAllTaiLieu.Name = "btnXemAllTaiLieu";
            this.btnXemAllTaiLieu.Size = new System.Drawing.Size(116, 33);
            this.btnXemAllTaiLieu.TabIndex = 18;
            this.btnXemAllTaiLieu.Text = "Xem Tất Cả";
            this.btnXemAllTaiLieu.UseVisualStyleBackColor = false;
            this.btnXemAllTaiLieu.Click += new System.EventHandler(this.btnXemAllTaiLieu_Click);
            // 
            // btnLapPhieuMuonTL
            // 
            this.btnLapPhieuMuonTL.BackColor = System.Drawing.Color.Green;
            this.btnLapPhieuMuonTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnLapPhieuMuonTL.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLapPhieuMuonTL.Location = new System.Drawing.Point(468, 348);
            this.btnLapPhieuMuonTL.Name = "btnLapPhieuMuonTL";
            this.btnLapPhieuMuonTL.Size = new System.Drawing.Size(142, 41);
            this.btnLapPhieuMuonTL.TabIndex = 15;
            this.btnLapPhieuMuonTL.Text = "Lập Phiếu Mượn";
            this.btnLapPhieuMuonTL.UseVisualStyleBackColor = false;
            this.btnLapPhieuMuonTL.Click += new System.EventHandler(this.btnLapPhieuMuonTL_Click);
            // 
            // btnXoaTL
            // 
            this.btnXoaTL.BackColor = System.Drawing.Color.Green;
            this.btnXoaTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnXoaTL.ForeColor = System.Drawing.Color.White;
            this.btnXoaTL.Location = new System.Drawing.Point(244, 348);
            this.btnXoaTL.Name = "btnXoaTL";
            this.btnXoaTL.Size = new System.Drawing.Size(117, 41);
            this.btnXoaTL.TabIndex = 14;
            this.btnXoaTL.Text = "Xóa";
            this.btnXoaTL.UseVisualStyleBackColor = false;
            this.btnXoaTL.Click += new System.EventHandler(this.btnXoaTL_Click);
            // 
            // btnXemChiTietTL
            // 
            this.btnXemChiTietTL.BackColor = System.Drawing.Color.Green;
            this.btnXemChiTietTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnXemChiTietTL.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXemChiTietTL.Location = new System.Drawing.Point(32, 347);
            this.btnXemChiTietTL.Name = "btnXemChiTietTL";
            this.btnXemChiTietTL.Size = new System.Drawing.Size(117, 41);
            this.btnXemChiTietTL.TabIndex = 13;
            this.btnXemChiTietTL.Text = "Xem Chi Tiết";
            this.btnXemChiTietTL.UseVisualStyleBackColor = false;
            this.btnXemChiTietTL.Click += new System.EventHandler(this.btnXemChiTietTL_Click);
            // 
            // dgvSearchTaiLieu
            // 
            this.dgvSearchTaiLieu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearchTaiLieu.Location = new System.Drawing.Point(20, 123);
            this.dgvSearchTaiLieu.Name = "dgvSearchTaiLieu";
            this.dgvSearchTaiLieu.Size = new System.Drawing.Size(840, 199);
            this.dgvSearchTaiLieu.TabIndex = 9;
            // 
            // btnSearchTaiLieu
            // 
            this.btnSearchTaiLieu.BackColor = System.Drawing.Color.Purple;
            this.btnSearchTaiLieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSearchTaiLieu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSearchTaiLieu.Location = new System.Drawing.Point(359, 55);
            this.btnSearchTaiLieu.Name = "btnSearchTaiLieu";
            this.btnSearchTaiLieu.Size = new System.Drawing.Size(123, 33);
            this.btnSearchTaiLieu.TabIndex = 8;
            this.btnSearchTaiLieu.Text = "Tìm Kiếm";
            this.btnSearchTaiLieu.UseVisualStyleBackColor = false;
            this.btnSearchTaiLieu.Click += new System.EventHandler(this.btnSearchTaiLieu_Click);
            // 
            // lblTenTaiLieu
            // 
            this.lblTenTaiLieu.AutoSize = true;
            this.lblTenTaiLieu.Location = new System.Drawing.Point(327, 23);
            this.lblTenTaiLieu.Name = "lblTenTaiLieu";
            this.lblTenTaiLieu.Size = new System.Drawing.Size(67, 13);
            this.lblTenTaiLieu.TabIndex = 6;
            this.lblTenTaiLieu.Text = "Tên Tài Liệu";
            // 
            // lblMaTaiLieu
            // 
            this.lblMaTaiLieu.AutoSize = true;
            this.lblMaTaiLieu.Location = new System.Drawing.Point(325, 22);
            this.lblMaTaiLieu.Name = "lblMaTaiLieu";
            this.lblMaTaiLieu.Size = new System.Drawing.Size(69, 13);
            this.lblMaTaiLieu.TabIndex = 5;
            this.lblMaTaiLieu.Text = "Mã Tài Liệu :";
            // 
            // rdTimTLNangCao
            // 
            this.rdTimTLNangCao.AutoSize = true;
            this.rdTimTLNangCao.Location = new System.Drawing.Point(192, 64);
            this.rdTimTLNangCao.Name = "rdTimTLNangCao";
            this.rdTimTLNangCao.Size = new System.Drawing.Size(93, 17);
            this.rdTimTLNangCao.TabIndex = 3;
            this.rdTimTLNangCao.TabStop = true;
            this.rdTimTLNangCao.Text = "Tìm Nâng Cao";
            this.rdTimTLNangCao.UseVisualStyleBackColor = true;
            this.rdTimTLNangCao.CheckedChanged += new System.EventHandler(this.rdTimTLNangCao_CheckedChanged);
            // 
            // rdTimTLCoBan
            // 
            this.rdTimTLCoBan.AutoSize = true;
            this.rdTimTLCoBan.Location = new System.Drawing.Point(192, 19);
            this.rdTimTLCoBan.Name = "rdTimTLCoBan";
            this.rdTimTLCoBan.Size = new System.Drawing.Size(80, 17);
            this.rdTimTLCoBan.TabIndex = 2;
            this.rdTimTLCoBan.TabStop = true;
            this.rdTimTLCoBan.Text = "Tìm Cơ Bản";
            this.rdTimTLCoBan.UseVisualStyleBackColor = true;
            this.rdTimTLCoBan.CheckedChanged += new System.EventHandler(this.rdTimTLCoBan_CheckedChanged);
            // 
            // txtSearchTaiLieu
            // 
            this.txtSearchTaiLieu.Location = new System.Drawing.Point(400, 20);
            this.txtSearchTaiLieu.Name = "txtSearchTaiLieu";
            this.txtSearchTaiLieu.Size = new System.Drawing.Size(122, 20);
            this.txtSearchTaiLieu.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(15, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 25);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tìm Kiếm Theo :";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.groupBox4);
            this.tabPage8.Location = new System.Drawing.Point(4, 29);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(903, 413);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Thêm Tài Liệu";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnThemTaiLieu);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.txtMaTL);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtSoLuongTL);
            this.groupBox4.Controls.Add(this.txtLoaiTL);
            this.groupBox4.Controls.Add(this.txtHienTrangTL);
            this.groupBox4.Controls.Add(this.txtTenTL);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Location = new System.Drawing.Point(28, 29);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox4.Size = new System.Drawing.Size(847, 355);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            // 
            // btnThemTaiLieu
            // 
            this.btnThemTaiLieu.BackColor = System.Drawing.Color.Red;
            this.btnThemTaiLieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnThemTaiLieu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnThemTaiLieu.Location = new System.Drawing.Point(536, 132);
            this.btnThemTaiLieu.Name = "btnThemTaiLieu";
            this.btnThemTaiLieu.Size = new System.Drawing.Size(114, 54);
            this.btnThemTaiLieu.TabIndex = 75;
            this.btnThemTaiLieu.Text = "Thêm";
            this.btnThemTaiLieu.UseVisualStyleBackColor = false;
            this.btnThemTaiLieu.Click += new System.EventHandler(this.btnThemTaiLieu_Click);
            // 
            // label4
            // 
            this.label4.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Location = new System.Drawing.Point(82, 273);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(2);
            this.label4.Size = new System.Drawing.Size(107, 54);
            this.label4.TabIndex = 74;
            this.label4.Text = "Số Lượng :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label4.UseCompatibleTextRendering = true;
            // 
            // txtMaTL
            // 
            this.txtMaTL.Location = new System.Drawing.Point(199, 28);
            this.txtMaTL.Name = "txtMaTL";
            this.txtMaTL.Size = new System.Drawing.Size(211, 20);
            this.txtMaTL.TabIndex = 73;
            // 
            // label5
            // 
            this.label5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Location = new System.Drawing.Point(82, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(2);
            this.label5.Size = new System.Drawing.Size(107, 54);
            this.label5.TabIndex = 72;
            this.label5.Text = "Mã Tài Liệu :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label5.UseCompatibleTextRendering = true;
            // 
            // txtSoLuongTL
            // 
            this.txtSoLuongTL.Location = new System.Drawing.Point(196, 292);
            this.txtSoLuongTL.Name = "txtSoLuongTL";
            this.txtSoLuongTL.Size = new System.Drawing.Size(211, 20);
            this.txtSoLuongTL.TabIndex = 60;
            // 
            // txtLoaiTL
            // 
            this.txtLoaiTL.Location = new System.Drawing.Point(196, 223);
            this.txtLoaiTL.Name = "txtLoaiTL";
            this.txtLoaiTL.Size = new System.Drawing.Size(211, 20);
            this.txtLoaiTL.TabIndex = 59;
            // 
            // txtHienTrangTL
            // 
            this.txtHienTrangTL.Location = new System.Drawing.Point(196, 150);
            this.txtHienTrangTL.Name = "txtHienTrangTL";
            this.txtHienTrangTL.Size = new System.Drawing.Size(211, 20);
            this.txtHienTrangTL.TabIndex = 58;
            // 
            // txtTenTL
            // 
            this.txtTenTL.Location = new System.Drawing.Point(199, 83);
            this.txtTenTL.Name = "txtTenTL";
            this.txtTenTL.Size = new System.Drawing.Size(211, 20);
            this.txtTenTL.TabIndex = 57;
            // 
            // label7
            // 
            this.label7.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label7.Location = new System.Drawing.Point(85, 132);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(2);
            this.label7.Size = new System.Drawing.Size(107, 54);
            this.label7.TabIndex = 44;
            this.label7.Text = "Hiện Trạng :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label7.UseCompatibleTextRendering = true;
            // 
            // label9
            // 
            this.label9.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label9.Location = new System.Drawing.Point(82, 204);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(2);
            this.label9.Size = new System.Drawing.Size(107, 54);
            this.label9.TabIndex = 42;
            this.label9.Text = "Loại Tài Liệu :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label9.UseCompatibleTextRendering = true;
            // 
            // label14
            // 
            this.label14.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label14.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Location = new System.Drawing.Point(85, 64);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Padding = new System.Windows.Forms.Padding(2);
            this.label14.Size = new System.Drawing.Size(104, 54);
            this.label14.TabIndex = 40;
            this.label14.Text = "Tên Tài Liệu :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label14.UseCompatibleTextRendering = true;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Mã Sách";
            this.columnHeader1.Width = 70;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tên Sách";
            this.columnHeader2.Width = 118;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "NXB";
            this.columnHeader3.Width = 77;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Tác Giả";
            this.columnHeader4.Width = 81;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Năm XB";
            this.columnHeader5.Width = 76;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Lần XB";
            this.columnHeader6.Width = 71;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Số lượng ";
            this.columnHeader7.Width = 76;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Nội dung";
            this.columnHeader8.Width = 130;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.btnNhanVien);
            this.groupBox2.Controls.Add(this.panelTraCuu);
            this.groupBox2.Controls.Add(this.btnDocGia);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.btnThongKe);
            this.groupBox2.Controls.Add(this.btnDangNhap);
            this.groupBox2.Location = new System.Drawing.Point(12, 116);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 462);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // btnNhanVien
            // 
            this.btnNhanVien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(154)))));
            this.btnNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnNhanVien.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnNhanVien.Location = new System.Drawing.Point(23, 128);
            this.btnNhanVien.Name = "btnNhanVien";
            this.btnNhanVien.Size = new System.Drawing.Size(145, 38);
            this.btnNhanVien.TabIndex = 15;
            this.btnNhanVien.Text = "Nhân Viên";
            this.btnNhanVien.UseVisualStyleBackColor = false;
            this.btnNhanVien.Click += new System.EventHandler(this.btnNhanVien_Click);
            // 
            // btnDocGia
            // 
            this.btnDocGia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(154)))));
            this.btnDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnDocGia.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnDocGia.Location = new System.Drawing.Point(23, 196);
            this.btnDocGia.Name = "btnDocGia";
            this.btnDocGia.Size = new System.Drawing.Size(145, 38);
            this.btnDocGia.TabIndex = 14;
            this.btnDocGia.Text = "Độc giả";
            this.btnDocGia.UseVisualStyleBackColor = false;
            this.btnDocGia.Click += new System.EventHandler(this.btnDocGia_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(28, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 13;
            this.label2.Text = "Danh Mục";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnThongKe
            // 
            this.btnThongKe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(154)))));
            this.btnThongKe.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnThongKe.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnThongKe.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnThongKe.Location = new System.Drawing.Point(23, 272);
            this.btnThongKe.Name = "btnThongKe";
            this.btnThongKe.Size = new System.Drawing.Size(145, 38);
            this.btnThongKe.TabIndex = 11;
            this.btnThongKe.Text = "Quản Lý Phiếu";
            this.btnThongKe.UseVisualStyleBackColor = false;
            this.btnThongKe.Click += new System.EventHandler(this.btnThongKe_Click);
            // 
            // btnDangNhap
            // 
            this.btnDangNhap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(154)))));
            this.btnDangNhap.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDangNhap.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnDangNhap.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDangNhap.Location = new System.Drawing.Point(23, 56);
            this.btnDangNhap.Name = "btnDangNhap";
            this.btnDangNhap.Size = new System.Drawing.Size(145, 38);
            this.btnDangNhap.TabIndex = 8;
            this.btnDangNhap.Text = "Quản Lý Sách";
            this.btnDangNhap.UseVisualStyleBackColor = false;
            this.btnDangNhap.Click += new System.EventHandler(this.btnDangNhap_Click);
            // 
            // tbcDocGia
            // 
            this.tbcDocGia.Controls.Add(this.tabPage7);
            this.tbcDocGia.Controls.Add(this.tabThemdg);
            this.tbcDocGia.ImageList = this.imageList1;
            this.tbcDocGia.ItemSize = new System.Drawing.Size(110, 25);
            this.tbcDocGia.Location = new System.Drawing.Point(0, 1);
            this.tbcDocGia.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.tbcDocGia.Name = "tbcDocGia";
            this.tbcDocGia.SelectedIndex = 0;
            this.tbcDocGia.Size = new System.Drawing.Size(891, 459);
            this.tbcDocGia.TabIndex = 0;
            this.tbcDocGia.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tab_DrawItem);
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage7.Controls.Add(this.cbxDinhDanh);
            this.tabPage7.Controls.Add(this.btnChinhSua);
            this.tabPage7.Controls.Add(this.btnHuy);
            this.tabPage7.Controls.Add(this.btnLuu);
            this.tabPage7.Controls.Add(this.btnXemAllDocGia);
            this.tabPage7.Controls.Add(this.btnLapPhieuCanhCao);
            this.tabPage7.Controls.Add(this.btnLapPhieuTra);
            this.tabPage7.Controls.Add(this.btnLapPhieMuon);
            this.tabPage7.Controls.Add(this.btnXoaDocGia);
            this.tabPage7.Controls.Add(this.btnXemChiTiet);
            this.tabPage7.Controls.Add(this.dgvDGSearch);
            this.tabPage7.Controls.Add(this.btnSearchDocGia);
            this.tabPage7.Controls.Add(this.lblHoTenSearch);
            this.tabPage7.Controls.Add(this.lblDinhDanhSearch);
            this.tabPage7.Controls.Add(this.lblMaDGSearch);
            this.tabPage7.Controls.Add(this.rdHoTenSearch);
            this.tabPage7.Controls.Add(this.rdMaDinhDanhSearch);
            this.tabPage7.Controls.Add(this.rdMaDGSearch);
            this.tabPage7.Controls.Add(this.txtSearchDG);
            this.tabPage7.Controls.Add(this.label30);
            this.tabPage7.Location = new System.Drawing.Point(4, 29);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(883, 426);
            this.tabPage7.TabIndex = 4;
            this.tabPage7.Text = "Tìm Kiếm Độc Giả";
            // 
            // cbxDinhDanh
            // 
            this.cbxDinhDanh.FormattingEnabled = true;
            this.cbxDinhDanh.Location = new System.Drawing.Point(417, 60);
            this.cbxDinhDanh.Name = "cbxDinhDanh";
            this.cbxDinhDanh.Size = new System.Drawing.Size(121, 21);
            this.cbxDinhDanh.TabIndex = 22;
            // 
            // btnChinhSua
            // 
            this.btnChinhSua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnChinhSua.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnChinhSua.ForeColor = System.Drawing.Color.White;
            this.btnChinhSua.Location = new System.Drawing.Point(120, 346);
            this.btnChinhSua.Name = "btnChinhSua";
            this.btnChinhSua.Size = new System.Drawing.Size(138, 43);
            this.btnChinhSua.TabIndex = 21;
            this.btnChinhSua.Text = "Chỉnh Sửa";
            this.btnChinhSua.UseVisualStyleBackColor = false;
            this.btnChinhSua.Click += new System.EventHandler(this.btnChinhSua_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnHuy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnHuy.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnHuy.Location = new System.Drawing.Point(553, 345);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(133, 44);
            this.btnHuy.TabIndex = 20;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = false;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnLuu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnLuu.ForeColor = System.Drawing.Color.White;
            this.btnLuu.Location = new System.Drawing.Point(328, 346);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(138, 43);
            this.btnLuu.TabIndex = 19;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.UseVisualStyleBackColor = false;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnXemAllDocGia
            // 
            this.btnXemAllDocGia.BackColor = System.Drawing.Color.Purple;
            this.btnXemAllDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnXemAllDocGia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXemAllDocGia.Location = new System.Drawing.Point(712, 64);
            this.btnXemAllDocGia.Name = "btnXemAllDocGia";
            this.btnXemAllDocGia.Size = new System.Drawing.Size(116, 33);
            this.btnXemAllDocGia.TabIndex = 18;
            this.btnXemAllDocGia.Text = "Xem Tất Cả";
            this.btnXemAllDocGia.UseVisualStyleBackColor = false;
            this.btnXemAllDocGia.Click += new System.EventHandler(this.btnXemAllDocGia_Click);
            // 
            // btnLapPhieuCanhCao
            // 
            this.btnLapPhieuCanhCao.BackColor = System.Drawing.Color.Green;
            this.btnLapPhieuCanhCao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnLapPhieuCanhCao.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLapPhieuCanhCao.Location = new System.Drawing.Point(672, 346);
            this.btnLapPhieuCanhCao.Name = "btnLapPhieuCanhCao";
            this.btnLapPhieuCanhCao.Size = new System.Drawing.Size(176, 41);
            this.btnLapPhieuCanhCao.TabIndex = 17;
            this.btnLapPhieuCanhCao.Text = "Lập Phiếu Cảnh Cáo";
            this.btnLapPhieuCanhCao.UseVisualStyleBackColor = false;
            // 
            // btnLapPhieuTra
            // 
            this.btnLapPhieuTra.BackColor = System.Drawing.Color.Green;
            this.btnLapPhieuTra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnLapPhieuTra.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLapPhieuTra.Location = new System.Drawing.Point(506, 346);
            this.btnLapPhieuTra.Name = "btnLapPhieuTra";
            this.btnLapPhieuTra.Size = new System.Drawing.Size(129, 41);
            this.btnLapPhieuTra.TabIndex = 16;
            this.btnLapPhieuTra.Text = "Lập Phiếu Trả";
            this.btnLapPhieuTra.UseVisualStyleBackColor = false;
            // 
            // btnLapPhieMuon
            // 
            this.btnLapPhieMuon.BackColor = System.Drawing.Color.Green;
            this.btnLapPhieMuon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnLapPhieMuon.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLapPhieMuon.Location = new System.Drawing.Point(328, 346);
            this.btnLapPhieMuon.Name = "btnLapPhieMuon";
            this.btnLapPhieMuon.Size = new System.Drawing.Size(142, 41);
            this.btnLapPhieMuon.TabIndex = 15;
            this.btnLapPhieMuon.Text = "Lập Phiếu Mượn";
            this.btnLapPhieMuon.UseVisualStyleBackColor = false;
            // 
            // btnXoaDocGia
            // 
            this.btnXoaDocGia.BackColor = System.Drawing.Color.Green;
            this.btnXoaDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnXoaDocGia.ForeColor = System.Drawing.Color.White;
            this.btnXoaDocGia.Location = new System.Drawing.Point(180, 346);
            this.btnXoaDocGia.Name = "btnXoaDocGia";
            this.btnXoaDocGia.Size = new System.Drawing.Size(117, 41);
            this.btnXoaDocGia.TabIndex = 14;
            this.btnXoaDocGia.Text = "Xóa";
            this.btnXoaDocGia.UseVisualStyleBackColor = false;
            this.btnXoaDocGia.Click += new System.EventHandler(this.btnXoaDocGia_Click);
            // 
            // btnXemChiTiet
            // 
            this.btnXemChiTiet.BackColor = System.Drawing.Color.Green;
            this.btnXemChiTiet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnXemChiTiet.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnXemChiTiet.Location = new System.Drawing.Point(30, 346);
            this.btnXemChiTiet.Name = "btnXemChiTiet";
            this.btnXemChiTiet.Size = new System.Drawing.Size(117, 41);
            this.btnXemChiTiet.TabIndex = 13;
            this.btnXemChiTiet.Text = "Xem Chi Tiết";
            this.btnXemChiTiet.UseVisualStyleBackColor = false;
            this.btnXemChiTiet.Click += new System.EventHandler(this.btnXemChiTiet_Click);
            // 
            // dgvDGSearch
            // 
            this.dgvDGSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDGSearch.Location = new System.Drawing.Point(20, 123);
            this.dgvDGSearch.Name = "dgvDGSearch";
            this.dgvDGSearch.Size = new System.Drawing.Size(840, 199);
            this.dgvDGSearch.TabIndex = 9;
            // 
            // btnSearchDocGia
            // 
            this.btnSearchDocGia.BackColor = System.Drawing.Color.Purple;
            this.btnSearchDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSearchDocGia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSearchDocGia.Location = new System.Drawing.Point(563, 64);
            this.btnSearchDocGia.Name = "btnSearchDocGia";
            this.btnSearchDocGia.Size = new System.Drawing.Size(123, 33);
            this.btnSearchDocGia.TabIndex = 8;
            this.btnSearchDocGia.Text = "Tìm Kiếm";
            this.btnSearchDocGia.UseVisualStyleBackColor = false;
            this.btnSearchDocGia.Click += new System.EventHandler(this.btnSearchDocGia_Click);
            // 
            // lblHoTenSearch
            // 
            this.lblHoTenSearch.AutoSize = true;
            this.lblHoTenSearch.Location = new System.Drawing.Point(574, 28);
            this.lblHoTenSearch.Name = "lblHoTenSearch";
            this.lblHoTenSearch.Size = new System.Drawing.Size(49, 13);
            this.lblHoTenSearch.TabIndex = 7;
            this.lblHoTenSearch.Text = "Họ Tên :";
            // 
            // lblDinhDanhSearch
            // 
            this.lblDinhDanhSearch.AutoSize = true;
            this.lblDinhDanhSearch.Location = new System.Drawing.Point(571, 28);
            this.lblDinhDanhSearch.Name = "lblDinhDanhSearch";
            this.lblDinhDanhSearch.Size = new System.Drawing.Size(115, 13);
            this.lblDinhDanhSearch.TabIndex = 6;
            this.lblDinhDanhSearch.Text = "CMND/MSSV/MSCB :";
            // 
            // lblMaDGSearch
            // 
            this.lblMaDGSearch.AutoSize = true;
            this.lblMaDGSearch.Location = new System.Drawing.Point(571, 28);
            this.lblMaDGSearch.Name = "lblMaDGSearch";
            this.lblMaDGSearch.Size = new System.Drawing.Size(70, 13);
            this.lblMaDGSearch.TabIndex = 5;
            this.lblMaDGSearch.Text = "Mã Độc Giả :";
            // 
            // rdHoTenSearch
            // 
            this.rdHoTenSearch.AutoSize = true;
            this.rdHoTenSearch.Location = new System.Drawing.Point(284, 100);
            this.rdHoTenSearch.Name = "rdHoTenSearch";
            this.rdHoTenSearch.Size = new System.Drawing.Size(61, 17);
            this.rdHoTenSearch.TabIndex = 4;
            this.rdHoTenSearch.TabStop = true;
            this.rdHoTenSearch.Text = "Họ Tên";
            this.rdHoTenSearch.UseVisualStyleBackColor = true;
            this.rdHoTenSearch.CheckedChanged += new System.EventHandler(this.rdHoTenSearch_CheckedChanged);
            // 
            // rdMaDinhDanhSearch
            // 
            this.rdMaDinhDanhSearch.AutoSize = true;
            this.rdMaDinhDanhSearch.Location = new System.Drawing.Point(284, 64);
            this.rdMaDinhDanhSearch.Name = "rdMaDinhDanhSearch";
            this.rdMaDinhDanhSearch.Size = new System.Drawing.Size(127, 17);
            this.rdMaDinhDanhSearch.TabIndex = 3;
            this.rdMaDinhDanhSearch.TabStop = true;
            this.rdMaDinhDanhSearch.Text = "CMND/MSSV/MSCB";
            this.rdMaDinhDanhSearch.UseVisualStyleBackColor = true;
            this.rdMaDinhDanhSearch.CheckedChanged += new System.EventHandler(this.rdMaDinhDanhSearch_CheckedChanged);
            // 
            // rdMaDGSearch
            // 
            this.rdMaDGSearch.AutoSize = true;
            this.rdMaDGSearch.Location = new System.Drawing.Point(284, 28);
            this.rdMaDGSearch.Name = "rdMaDGSearch";
            this.rdMaDGSearch.Size = new System.Drawing.Size(82, 17);
            this.rdMaDGSearch.TabIndex = 2;
            this.rdMaDGSearch.TabStop = true;
            this.rdMaDGSearch.Text = "Mã Độc Giả";
            this.rdMaDGSearch.UseVisualStyleBackColor = true;
            this.rdMaDGSearch.CheckedChanged += new System.EventHandler(this.rdMaDGSearch_CheckedChanged);
            // 
            // txtSearchDG
            // 
            this.txtSearchDG.Location = new System.Drawing.Point(696, 25);
            this.txtSearchDG.Name = "txtSearchDG";
            this.txtSearchDG.Size = new System.Drawing.Size(122, 20);
            this.txtSearchDG.TabIndex = 1;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label30.Location = new System.Drawing.Point(90, 42);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(168, 25);
            this.label30.TabIndex = 0;
            this.label30.Text = "Tìm Kiếm Theo :";
            // 
            // tabThemdg
            // 
            this.tabThemdg.Controls.Add(this.btnDangKyDocGia);
            this.tabThemdg.Controls.Add(this.groupBox3);
            this.tabThemdg.Location = new System.Drawing.Point(4, 29);
            this.tabThemdg.Name = "tabThemdg";
            this.tabThemdg.Padding = new System.Windows.Forms.Padding(3);
            this.tabThemdg.Size = new System.Drawing.Size(883, 426);
            this.tabThemdg.TabIndex = 1;
            this.tabThemdg.Text = "Thêm Đọc Giả";
            this.tabThemdg.UseVisualStyleBackColor = true;
            // 
            // btnDangKyDocGia
            // 
            this.btnDangKyDocGia.BackColor = System.Drawing.Color.SeaGreen;
            this.btnDangKyDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnDangKyDocGia.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnDangKyDocGia.Location = new System.Drawing.Point(663, 328);
            this.btnDangKyDocGia.Name = "btnDangKyDocGia";
            this.btnDangKyDocGia.Size = new System.Drawing.Size(157, 35);
            this.btnDangKyDocGia.TabIndex = 71;
            this.btnDangKyDocGia.Text = "Đăng Ký";
            this.btnDangKyDocGia.UseVisualStyleBackColor = false;
            this.btnDangKyDocGia.Click += new System.EventHandler(this.btnDangKyDocGia_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtMaDG);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.lblMSCBDangKy);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.lblMSSVDangKy);
            this.groupBox3.Controls.Add(this.rdKhac);
            this.groupBox3.Controls.Add(this.rdCanBo);
            this.groupBox3.Controls.Add(this.rdSinhVien);
            this.groupBox3.Controls.Add(this.txtCMNDDG);
            this.groupBox3.Controls.Add(this.txtMSCBDG);
            this.groupBox3.Controls.Add(this.txtMSSVDG);
            this.groupBox3.Controls.Add(this.txtEmailDG);
            this.groupBox3.Controls.Add(this.txtSDTDG);
            this.groupBox3.Controls.Add(this.txtDiaChiDG);
            this.groupBox3.Controls.Add(this.txtNgaySinhDG);
            this.groupBox3.Controls.Add(this.txtTenDG);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.lbUserName);
            this.groupBox3.Location = new System.Drawing.Point(27, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox3.Size = new System.Drawing.Size(847, 355);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // txtMaDG
            // 
            this.txtMaDG.Location = new System.Drawing.Point(199, 28);
            this.txtMaDG.Name = "txtMaDG";
            this.txtMaDG.Size = new System.Drawing.Size(211, 20);
            this.txtMaDG.TabIndex = 73;
            // 
            // label29
            // 
            this.label29.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label29.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label29.Image = ((System.Drawing.Image)(resources.GetObject("label29.Image")));
            this.label29.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label29.Location = new System.Drawing.Point(45, 9);
            this.label29.Margin = new System.Windows.Forms.Padding(0);
            this.label29.Name = "label29";
            this.label29.Padding = new System.Windows.Forms.Padding(2);
            this.label29.Size = new System.Drawing.Size(147, 54);
            this.label29.TabIndex = 72;
            this.label29.Text = "Mã Độc Giả :";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label29.UseCompatibleTextRendering = true;
            // 
            // label27
            // 
            this.label27.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label27.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label27.Image = ((System.Drawing.Image)(resources.GetObject("label27.Image")));
            this.label27.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label27.Location = new System.Drawing.Point(472, 86);
            this.label27.Margin = new System.Windows.Forms.Padding(0);
            this.label27.Name = "label27";
            this.label27.Padding = new System.Windows.Forms.Padding(2);
            this.label27.Size = new System.Drawing.Size(124, 54);
            this.label27.TabIndex = 71;
            this.label27.Text = "CMND :";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label27.UseCompatibleTextRendering = true;
            // 
            // lblMSCBDangKy
            // 
            this.lblMSCBDangKy.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lblMSCBDangKy.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblMSCBDangKy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMSCBDangKy.Location = new System.Drawing.Point(495, 229);
            this.lblMSCBDangKy.Margin = new System.Windows.Forms.Padding(0);
            this.lblMSCBDangKy.Name = "lblMSCBDangKy";
            this.lblMSCBDangKy.Padding = new System.Windows.Forms.Padding(2);
            this.lblMSCBDangKy.Size = new System.Drawing.Size(60, 44);
            this.lblMSCBDangKy.TabIndex = 69;
            this.lblMSCBDangKy.Text = "MSCB";
            this.lblMSCBDangKy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblMSCBDangKy.UseCompatibleTextRendering = true;
            // 
            // label12
            // 
            this.label12.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label12.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label12.Image = ((System.Drawing.Image)(resources.GetObject("label12.Image")));
            this.label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label12.Location = new System.Drawing.Point(45, 273);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Padding = new System.Windows.Forms.Padding(2);
            this.label12.Size = new System.Drawing.Size(126, 54);
            this.label12.TabIndex = 43;
            this.label12.Text = "SĐT :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label12.UseCompatibleTextRendering = true;
            // 
            // lblMSSVDangKy
            // 
            this.lblMSSVDangKy.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lblMSSVDangKy.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lblMSSVDangKy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMSSVDangKy.Location = new System.Drawing.Point(495, 229);
            this.lblMSSVDangKy.Margin = new System.Windows.Forms.Padding(0);
            this.lblMSSVDangKy.Name = "lblMSSVDangKy";
            this.lblMSSVDangKy.Padding = new System.Windows.Forms.Padding(2);
            this.lblMSSVDangKy.Size = new System.Drawing.Size(60, 44);
            this.lblMSSVDangKy.TabIndex = 68;
            this.lblMSSVDangKy.Text = "MSSV:";
            this.lblMSSVDangKy.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblMSSVDangKy.UseCompatibleTextRendering = true;
            // 
            // rdKhac
            // 
            this.rdKhac.AutoSize = true;
            this.rdKhac.Location = new System.Drawing.Point(649, 220);
            this.rdKhac.Name = "rdKhac";
            this.rdKhac.Size = new System.Drawing.Size(50, 17);
            this.rdKhac.TabIndex = 67;
            this.rdKhac.TabStop = true;
            this.rdKhac.Text = "Khác";
            this.rdKhac.UseVisualStyleBackColor = true;
            this.rdKhac.CheckedChanged += new System.EventHandler(this.rdKhac_CheckedChanged);
            // 
            // rdCanBo
            // 
            this.rdCanBo.AutoSize = true;
            this.rdCanBo.Location = new System.Drawing.Point(648, 183);
            this.rdCanBo.Name = "rdCanBo";
            this.rdCanBo.Size = new System.Drawing.Size(121, 17);
            this.rdCanBo.TabIndex = 66;
            this.rdCanBo.TabStop = true;
            this.rdCanBo.Text = "Cán Bộ / Nhân Viên";
            this.rdCanBo.UseVisualStyleBackColor = true;
            this.rdCanBo.CheckedChanged += new System.EventHandler(this.rdCanBo_CheckedChanged);
            // 
            // rdSinhVien
            // 
            this.rdSinhVien.AutoSize = true;
            this.rdSinhVien.Location = new System.Drawing.Point(649, 144);
            this.rdSinhVien.Name = "rdSinhVien";
            this.rdSinhVien.Size = new System.Drawing.Size(70, 17);
            this.rdSinhVien.TabIndex = 65;
            this.rdSinhVien.TabStop = true;
            this.rdSinhVien.Text = "Sinh Viên";
            this.rdSinhVien.UseVisualStyleBackColor = true;
            this.rdSinhVien.CheckedChanged += new System.EventHandler(this.rdSinhVien_CheckedChanged);
            // 
            // txtCMNDDG
            // 
            this.txtCMNDDG.Location = new System.Drawing.Point(601, 105);
            this.txtCMNDDG.Name = "txtCMNDDG";
            this.txtCMNDDG.Size = new System.Drawing.Size(211, 20);
            this.txtCMNDDG.TabIndex = 64;
            // 
            // txtMSCBDG
            // 
            this.txtMSCBDG.Location = new System.Drawing.Point(571, 244);
            this.txtMSCBDG.Name = "txtMSCBDG";
            this.txtMSCBDG.Size = new System.Drawing.Size(211, 20);
            this.txtMSCBDG.TabIndex = 63;
            // 
            // txtMSSVDG
            // 
            this.txtMSSVDG.Location = new System.Drawing.Point(571, 244);
            this.txtMSSVDG.Name = "txtMSSVDG";
            this.txtMSSVDG.Size = new System.Drawing.Size(211, 20);
            this.txtMSSVDG.TabIndex = 62;
            // 
            // txtEmailDG
            // 
            this.txtEmailDG.Location = new System.Drawing.Point(601, 28);
            this.txtEmailDG.Name = "txtEmailDG";
            this.txtEmailDG.Size = new System.Drawing.Size(211, 20);
            this.txtEmailDG.TabIndex = 61;
            // 
            // txtSDTDG
            // 
            this.txtSDTDG.Location = new System.Drawing.Point(196, 292);
            this.txtSDTDG.Name = "txtSDTDG";
            this.txtSDTDG.Size = new System.Drawing.Size(211, 20);
            this.txtSDTDG.TabIndex = 60;
            // 
            // txtDiaChiDG
            // 
            this.txtDiaChiDG.Location = new System.Drawing.Point(196, 223);
            this.txtDiaChiDG.Name = "txtDiaChiDG";
            this.txtDiaChiDG.Size = new System.Drawing.Size(211, 20);
            this.txtDiaChiDG.TabIndex = 59;
            // 
            // txtNgaySinhDG
            // 
            this.txtNgaySinhDG.Location = new System.Drawing.Point(196, 150);
            this.txtNgaySinhDG.Name = "txtNgaySinhDG";
            this.txtNgaySinhDG.Size = new System.Drawing.Size(211, 20);
            this.txtNgaySinhDG.TabIndex = 58;
            // 
            // txtTenDG
            // 
            this.txtTenDG.Location = new System.Drawing.Point(199, 83);
            this.txtTenDG.Name = "txtTenDG";
            this.txtTenDG.Size = new System.Drawing.Size(211, 20);
            this.txtTenDG.TabIndex = 57;
            // 
            // label6
            // 
            this.label6.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label6.Image = ((System.Drawing.Image)(resources.GetObject("label6.Image")));
            this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Location = new System.Drawing.Point(481, 163);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(2);
            this.label6.Size = new System.Drawing.Size(144, 54);
            this.label6.TabIndex = 52;
            this.label6.Text = "Loại Độc Gải :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label6.UseCompatibleTextRendering = true;
            // 
            // label13
            // 
            this.label13.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label13.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label13.Image = ((System.Drawing.Image)(resources.GetObject("label13.Image")));
            this.label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Location = new System.Drawing.Point(45, 132);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Padding = new System.Windows.Forms.Padding(2);
            this.label13.Size = new System.Drawing.Size(147, 54);
            this.label13.TabIndex = 44;
            this.label13.Text = "Ngày Sinh :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label13.UseCompatibleTextRendering = true;
            // 
            // label11
            // 
            this.label11.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label11.Image = ((System.Drawing.Image)(resources.GetObject("label11.Image")));
            this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label11.Location = new System.Drawing.Point(45, 205);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Padding = new System.Windows.Forms.Padding(2);
            this.label11.Size = new System.Drawing.Size(137, 54);
            this.label11.TabIndex = 42;
            this.label11.Text = "Địa Chỉ :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label11.UseCompatibleTextRendering = true;
            // 
            // label10
            // 
            this.label10.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label10.Image = ((System.Drawing.Image)(resources.GetObject("label10.Image")));
            this.label10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label10.Location = new System.Drawing.Point(469, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Padding = new System.Windows.Forms.Padding(2);
            this.label10.Size = new System.Drawing.Size(124, 54);
            this.label10.TabIndex = 41;
            this.label10.Text = "Email :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label10.UseCompatibleTextRendering = true;
            // 
            // lbUserName
            // 
            this.lbUserName.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lbUserName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbUserName.Image = ((System.Drawing.Image)(resources.GetObject("lbUserName.Image")));
            this.lbUserName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbUserName.Location = new System.Drawing.Point(42, 71);
            this.lbUserName.Margin = new System.Windows.Forms.Padding(0);
            this.lbUserName.Name = "lbUserName";
            this.lbUserName.Padding = new System.Windows.Forms.Padding(2);
            this.lbUserName.Size = new System.Drawing.Size(147, 54);
            this.lbUserName.TabIndex = 40;
            this.lbUserName.Text = "Tên Đọc Giả :";
            this.lbUserName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbUserName.UseCompatibleTextRendering = true;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "icons8-user (1).png");
            this.imageList1.Images.SetKeyName(1, "icons8-add-user-male (2).png");
            this.imageList1.Images.SetKeyName(2, "icons8-user-location (1).png");
            this.imageList1.Images.SetKeyName(3, "icons8-delete-shield (1).png");
            // 
            // panelDocGia
            // 
            this.panelDocGia.Controls.Add(this.tbcDocGia);
            this.panelDocGia.Location = new System.Drawing.Point(219, 45);
            this.panelDocGia.Name = "panelDocGia";
            this.panelDocGia.Size = new System.Drawing.Size(897, 460);
            this.panelDocGia.TabIndex = 3;
            // 
            // panelQLNhanVien
            // 
            this.panelQLNhanVien.Controls.Add(this.tbcQuanLiNV);
            this.panelQLNhanVien.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.panelQLNhanVien.Location = new System.Drawing.Point(1092, 37);
            this.panelQLNhanVien.Name = "panelQLNhanVien";
            this.panelQLNhanVien.Size = new System.Drawing.Size(900, 445);
            this.panelQLNhanVien.TabIndex = 2;
            this.panelQLNhanVien.Visible = false;
            // 
            // tbcQuanLiNV
            // 
            this.tbcQuanLiNV.Controls.Add(this.tabPage4);
            this.tbcQuanLiNV.Controls.Add(this.tabPage5);
            this.tbcQuanLiNV.Controls.Add(this.tabPage6);
            this.tbcQuanLiNV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcQuanLiNV.ItemSize = new System.Drawing.Size(110, 25);
            this.tbcQuanLiNV.Location = new System.Drawing.Point(0, 0);
            this.tbcQuanLiNV.Name = "tbcQuanLiNV";
            this.tbcQuanLiNV.SelectedIndex = 0;
            this.tbcQuanLiNV.Size = new System.Drawing.Size(900, 445);
            this.tbcQuanLiNV.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgvNhanVien);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(892, 412);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Danh Sách Nhân Viên";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgvNhanVien
            // 
            this.dgvNhanVien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNhanVien.Location = new System.Drawing.Point(3, 3);
            this.dgvNhanVien.Name = "dgvNhanVien";
            this.dgvNhanVien.Size = new System.Drawing.Size(880, 400);
            this.dgvNhanVien.TabIndex = 0;
            this.dgvNhanVien.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNhanVien_CellContentClick);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.txtMatKhauNVCapNhat);
            this.tabPage5.Controls.Add(this.label21);
            this.tabPage5.Controls.Add(this.rdAdminCapNhat);
            this.tabPage5.Controls.Add(this.rdThuThuCapNhat);
            this.tabPage5.Controls.Add(this.label20);
            this.tabPage5.Controls.Add(this.btnCapNhatNhanVien);
            this.tabPage5.Controls.Add(this.txtHoTenNVCapNhat);
            this.tabPage5.Controls.Add(this.label19);
            this.tabPage5.Controls.Add(this.txtTenDangNhapNVCapNhat);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.txtCaTrucNVCapNhat);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Controls.Add(this.txtMaNVCapNhap);
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(892, 412);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "Cập Nhật Nhân Viên";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // txtMatKhauNVCapNhat
            // 
            this.txtMatKhauNVCapNhat.Location = new System.Drawing.Point(202, 187);
            this.txtMatKhauNVCapNhat.Name = "txtMatKhauNVCapNhat";
            this.txtMatKhauNVCapNhat.Size = new System.Drawing.Size(207, 20);
            this.txtMatKhauNVCapNhat.TabIndex = 13;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(71, 190);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 13);
            this.label21.TabIndex = 12;
            this.label21.Text = "Mật Khẩu :";
            // 
            // rdAdminCapNhat
            // 
            this.rdAdminCapNhat.AutoSize = true;
            this.rdAdminCapNhat.Location = new System.Drawing.Point(202, 310);
            this.rdAdminCapNhat.Name = "rdAdminCapNhat";
            this.rdAdminCapNhat.Size = new System.Drawing.Size(54, 17);
            this.rdAdminCapNhat.TabIndex = 11;
            this.rdAdminCapNhat.TabStop = true;
            this.rdAdminCapNhat.Text = "Admin";
            this.rdAdminCapNhat.UseVisualStyleBackColor = true;
            // 
            // rdThuThuCapNhat
            // 
            this.rdThuThuCapNhat.AutoSize = true;
            this.rdThuThuCapNhat.Location = new System.Drawing.Point(202, 278);
            this.rdThuThuCapNhat.Name = "rdThuThuCapNhat";
            this.rdThuThuCapNhat.Size = new System.Drawing.Size(66, 17);
            this.rdThuThuCapNhat.TabIndex = 10;
            this.rdThuThuCapNhat.TabStop = true;
            this.rdThuThuCapNhat.Text = "Thủ Thư";
            this.rdThuThuCapNhat.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(71, 290);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "Loại Nhân Viên :";
            // 
            // btnCapNhatNhanVien
            // 
            this.btnCapNhatNhanVien.BackColor = System.Drawing.Color.OrangeRed;
            this.btnCapNhatNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnCapNhatNhanVien.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCapNhatNhanVien.Location = new System.Drawing.Point(299, 340);
            this.btnCapNhatNhanVien.Name = "btnCapNhatNhanVien";
            this.btnCapNhatNhanVien.Size = new System.Drawing.Size(135, 45);
            this.btnCapNhatNhanVien.TabIndex = 8;
            this.btnCapNhatNhanVien.Text = "Cập Nhật";
            this.btnCapNhatNhanVien.UseVisualStyleBackColor = false;
            this.btnCapNhatNhanVien.Click += new System.EventHandler(this.btnCapNhatNhanVien_Click);
            // 
            // txtHoTenNVCapNhat
            // 
            this.txtHoTenNVCapNhat.Location = new System.Drawing.Point(202, 90);
            this.txtHoTenNVCapNhat.Name = "txtHoTenNVCapNhat";
            this.txtHoTenNVCapNhat.Size = new System.Drawing.Size(204, 20);
            this.txtHoTenNVCapNhat.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(71, 93);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(49, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Họ Tên :";
            // 
            // txtTenDangNhapNVCapNhat
            // 
            this.txtTenDangNhapNVCapNhat.Location = new System.Drawing.Point(202, 138);
            this.txtTenDangNhapNVCapNhat.Name = "txtTenDangNhapNVCapNhat";
            this.txtTenDangNhapNVCapNhat.Size = new System.Drawing.Size(204, 20);
            this.txtTenDangNhapNVCapNhat.TabIndex = 5;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(71, 138);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Tên Đăng Nhập :";
            // 
            // txtCaTrucNVCapNhat
            // 
            this.txtCaTrucNVCapNhat.Location = new System.Drawing.Point(202, 231);
            this.txtCaTrucNVCapNhat.Name = "txtCaTrucNVCapNhat";
            this.txtCaTrucNVCapNhat.Size = new System.Drawing.Size(207, 20);
            this.txtCaTrucNVCapNhat.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(71, 234);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Ca Trực :";
            // 
            // txtMaNVCapNhap
            // 
            this.txtMaNVCapNhap.Location = new System.Drawing.Point(202, 48);
            this.txtMaNVCapNhap.Name = "txtMaNVCapNhap";
            this.txtMaNVCapNhap.Size = new System.Drawing.Size(207, 20);
            this.txtMaNVCapNhap.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(71, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Mã Nhân Viên : ";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.btnXoaNhanVien);
            this.tabPage6.Controls.Add(this.txtMaNVXoa);
            this.tabPage6.Controls.Add(this.label22);
            this.tabPage6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(892, 412);
            this.tabPage6.TabIndex = 4;
            this.tabPage6.Text = "Xóa Nhân Viên";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // btnXoaNhanVien
            // 
            this.btnXoaNhanVien.BackColor = System.Drawing.Color.Red;
            this.btnXoaNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnXoaNhanVien.ForeColor = System.Drawing.SystemColors.Control;
            this.btnXoaNhanVien.Location = new System.Drawing.Point(189, 176);
            this.btnXoaNhanVien.Name = "btnXoaNhanVien";
            this.btnXoaNhanVien.Size = new System.Drawing.Size(115, 38);
            this.btnXoaNhanVien.TabIndex = 2;
            this.btnXoaNhanVien.Text = "Xóa";
            this.btnXoaNhanVien.UseVisualStyleBackColor = false;
            this.btnXoaNhanVien.Click += new System.EventHandler(this.btnXoaNhanVien_Click);
            // 
            // txtMaNVXoa
            // 
            this.txtMaNVXoa.Location = new System.Drawing.Point(145, 122);
            this.txtMaNVXoa.Name = "txtMaNVXoa";
            this.txtMaNVXoa.Size = new System.Drawing.Size(187, 20);
            this.txtMaNVXoa.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label22.Location = new System.Drawing.Point(118, 68);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(252, 25);
            this.label22.TabIndex = 0;
            this.label22.Text = "Mã Nhân Viên Muốn Xóa";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(44, 25);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(73, 57);
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "icons8-book.png");
            this.imageList2.Images.SetKeyName(1, "icons8-bookmark-ribbon.png");
            this.imageList2.Images.SetKeyName(2, "icons8-books.png");
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "icons8-male-user.png");
            this.imageList3.Images.SetKeyName(1, "icons8-change-user.png");
            this.imageList3.Images.SetKeyName(2, "icons8-trash-can.png");
            // 
            // pnThongKe
            // 
            this.pnThongKe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(154)))));
            this.pnThongKe.Controls.Add(this.pnthu);
            this.pnThongKe.Controls.Add(this.tabControl1);
            this.pnThongKe.Location = new System.Drawing.Point(186, 57);
            this.pnThongKe.Name = "pnThongKe";
            this.pnThongKe.Size = new System.Drawing.Size(871, 463);
            this.pnThongKe.TabIndex = 15;
            // 
            // pnthu
            // 
            this.pnthu.Location = new System.Drawing.Point(868, 0);
            this.pnthu.Name = "pnthu";
            this.pnthu.Size = new System.Drawing.Size(39, 459);
            this.pnthu.TabIndex = 9;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPhieuMuon);
            this.tabControl1.Controls.Add(this.tabPhieuTra);
            this.tabControl1.Controls.Add(this.tabPhieuPhat);
            this.tabControl1.Controls.Add(this.tabPhieuNhacNho);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.ItemSize = new System.Drawing.Size(110, 25);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(871, 463);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pnLapPhieumuon);
            this.tabPage2.Controls.Add(this.pnxemphieumuon);
            this.tabPage2.Controls.Add(this.btnXmuon);
            this.tabPage2.Controls.Add(this.btnSmuon);
            this.tabPage2.Controls.Add(this.btnCNmuon);
            this.tabPage2.Controls.Add(this.btnXPmuon);
            this.tabPage2.Controls.Add(this.btnLPmuon);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(863, 430);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Phiếu mượn";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pnLapPhieumuon
            // 
            this.pnLapPhieumuon.Controls.Add(this.btnLuuPhieuMuon);
            this.pnLapPhieumuon.Controls.Add(this.textBox11);
            this.pnLapPhieumuon.Controls.Add(this.label43);
            this.pnLapPhieumuon.Controls.Add(this.dtphantraphieumuon);
            this.pnLapPhieumuon.Controls.Add(this.label40);
            this.pnLapPhieumuon.Controls.Add(this.txtMatailieumuon);
            this.pnLapPhieumuon.Controls.Add(this.label41);
            this.pnLapPhieumuon.Controls.Add(this.txtSTTmuon);
            this.pnLapPhieumuon.Controls.Add(this.label42);
            this.pnLapPhieumuon.Controls.Add(this.dtpNgayLapphieumuon);
            this.pnLapPhieumuon.Controls.Add(this.label28);
            this.pnLapPhieumuon.Controls.Add(this.txtMadocgiamuon);
            this.pnLapPhieumuon.Controls.Add(this.label16);
            this.pnLapPhieumuon.Controls.Add(this.txtMaphieumuon);
            this.pnLapPhieumuon.Controls.Add(this.label15);
            this.pnLapPhieumuon.Location = new System.Drawing.Point(0, 24);
            this.pnLapPhieumuon.Name = "pnLapPhieumuon";
            this.pnLapPhieumuon.Size = new System.Drawing.Size(860, 383);
            this.pnLapPhieumuon.TabIndex = 6;
            // 
            // btnLuuPhieuMuon
            // 
            this.btnLuuPhieuMuon.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuPhieuMuon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(244)))));
            this.btnLuuPhieuMuon.Location = new System.Drawing.Point(367, 254);
            this.btnLuuPhieuMuon.Name = "btnLuuPhieuMuon";
            this.btnLuuPhieuMuon.Size = new System.Drawing.Size(143, 36);
            this.btnLuuPhieuMuon.TabIndex = 17;
            this.btnLuuPhieuMuon.Text = "save";
            this.btnLuuPhieuMuon.UseVisualStyleBackColor = true;
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(679, 97);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(63, 26);
            this.textBox11.TabIndex = 16;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(641, 102);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label43.Size = new System.Drawing.Size(32, 18);
            this.label43.TabIndex = 15;
            this.label43.Text = ",SL:";
            // 
            // dtphantraphieumuon
            // 
            this.dtphantraphieumuon.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtphantraphieumuon.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtphantraphieumuon.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtphantraphieumuon.Location = new System.Drawing.Point(542, 153);
            this.dtphantraphieumuon.Name = "dtphantraphieumuon";
            this.dtphantraphieumuon.Size = new System.Drawing.Size(200, 26);
            this.dtphantraphieumuon.TabIndex = 14;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(452, 159);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(62, 18);
            this.label40.TabIndex = 13;
            this.label40.Text = "Hạn trả:";
            // 
            // txtMatailieumuon
            // 
            this.txtMatailieumuon.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMatailieumuon.Location = new System.Drawing.Point(542, 97);
            this.txtMatailieumuon.Name = "txtMatailieumuon";
            this.txtMatailieumuon.Size = new System.Drawing.Size(86, 26);
            this.txtMatailieumuon.TabIndex = 12;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(452, 102);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(81, 18);
            this.label41.TabIndex = 11;
            this.label41.Text = "Mã tài liệu:";
            // 
            // txtSTTmuon
            // 
            this.txtSTTmuon.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSTTmuon.Location = new System.Drawing.Point(542, 44);
            this.txtSTTmuon.Name = "txtSTTmuon";
            this.txtSTTmuon.Size = new System.Drawing.Size(200, 26);
            this.txtSTTmuon.TabIndex = 10;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(452, 49);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(84, 18);
            this.label42.TabIndex = 9;
            this.label42.Text = "STT mượn:";
            // 
            // dtpNgayLapphieumuon
            // 
            this.dtpNgayLapphieumuon.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayLapphieumuon.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayLapphieumuon.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayLapphieumuon.Location = new System.Drawing.Point(166, 154);
            this.dtpNgayLapphieumuon.Name = "dtpNgayLapphieumuon";
            this.dtpNgayLapphieumuon.Size = new System.Drawing.Size(200, 26);
            this.dtpNgayLapphieumuon.TabIndex = 8;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(16, 154);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(109, 18);
            this.label28.TabIndex = 7;
            this.label28.Text = "Ngày lập phiếu:";
            // 
            // txtMadocgiamuon
            // 
            this.txtMadocgiamuon.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMadocgiamuon.Location = new System.Drawing.Point(166, 99);
            this.txtMadocgiamuon.Name = "txtMadocgiamuon";
            this.txtMadocgiamuon.Size = new System.Drawing.Size(200, 26);
            this.txtMadocgiamuon.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(16, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 18);
            this.label16.TabIndex = 5;
            this.label16.Text = "Mã độc giả:";
            // 
            // txtMaphieumuon
            // 
            this.txtMaphieumuon.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaphieumuon.Location = new System.Drawing.Point(166, 45);
            this.txtMaphieumuon.Name = "txtMaphieumuon";
            this.txtMaphieumuon.Size = new System.Drawing.Size(200, 26);
            this.txtMaphieumuon.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(16, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 18);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã phiếu:";
            // 
            // pnxemphieumuon
            // 
            this.pnxemphieumuon.Controls.Add(this.pnNhapthongtintimkiemphieumuon);
            this.pnxemphieumuon.Controls.Add(this.dgvxemphieumuon);
            this.pnxemphieumuon.Location = new System.Drawing.Point(0, 24);
            this.pnxemphieumuon.Name = "pnxemphieumuon";
            this.pnxemphieumuon.Size = new System.Drawing.Size(860, 410);
            this.pnxemphieumuon.TabIndex = 5;
            // 
            // pnNhapthongtintimkiemphieumuon
            // 
            this.pnNhapthongtintimkiemphieumuon.Controls.Add(this.groupBox5);
            this.pnNhapthongtintimkiemphieumuon.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnNhapthongtintimkiemphieumuon.Location = new System.Drawing.Point(0, 0);
            this.pnNhapthongtintimkiemphieumuon.Name = "pnNhapthongtintimkiemphieumuon";
            this.pnNhapthongtintimkiemphieumuon.Size = new System.Drawing.Size(860, 91);
            this.pnNhapthongtintimkiemphieumuon.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Font = new System.Drawing.Font("Constantia", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(244)))));
            this.groupBox5.Location = new System.Drawing.Point(0, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(860, 81);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tìm kiếm phiếu mượn:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(531, 22);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(147, 24);
            this.textBox2.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(155, 24);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(147, 24);
            this.textBox1.TabIndex = 2;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(410, 27);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(116, 17);
            this.label45.TabIndex = 1;
            this.label45.Text = "Nhập mã độc giả:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(9, 29);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(149, 17);
            this.label44.TabIndex = 0;
            this.label44.Text = "Nhập mã phiếu mượn:";
            // 
            // dgvxemphieumuon
            // 
            this.dgvxemphieumuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvxemphieumuon.Location = new System.Drawing.Point(-1, 93);
            this.dgvxemphieumuon.Name = "dgvxemphieumuon";
            this.dgvxemphieumuon.Size = new System.Drawing.Size(860, 287);
            this.dgvxemphieumuon.TabIndex = 0;
            // 
            // btnXmuon
            // 
            this.btnXmuon.Location = new System.Drawing.Point(305, 2);
            this.btnXmuon.Name = "btnXmuon";
            this.btnXmuon.Size = new System.Drawing.Size(75, 23);
            this.btnXmuon.TabIndex = 4;
            this.btnXmuon.Text = "Xóa";
            this.btnXmuon.UseVisualStyleBackColor = true;
            // 
            // btnSmuon
            // 
            this.btnSmuon.Location = new System.Drawing.Point(233, 2);
            this.btnSmuon.Name = "btnSmuon";
            this.btnSmuon.Size = new System.Drawing.Size(75, 23);
            this.btnSmuon.TabIndex = 3;
            this.btnSmuon.Text = "Sửa";
            this.btnSmuon.UseVisualStyleBackColor = true;
            // 
            // btnCNmuon
            // 
            this.btnCNmuon.Location = new System.Drawing.Point(161, 2);
            this.btnCNmuon.Name = "btnCNmuon";
            this.btnCNmuon.Size = new System.Drawing.Size(75, 23);
            this.btnCNmuon.TabIndex = 2;
            this.btnCNmuon.Text = "Tìm kiếm";
            this.btnCNmuon.UseVisualStyleBackColor = true;
            this.btnCNmuon.Click += new System.EventHandler(this.btnCNmuon_Click);
            // 
            // btnXPmuon
            // 
            this.btnXPmuon.Location = new System.Drawing.Point(72, 2);
            this.btnXPmuon.Name = "btnXPmuon";
            this.btnXPmuon.Size = new System.Drawing.Size(91, 23);
            this.btnXPmuon.TabIndex = 1;
            this.btnXPmuon.Text = "Xem danh sách";
            this.btnXPmuon.UseVisualStyleBackColor = true;
            this.btnXPmuon.Click += new System.EventHandler(this.btnXPmuon_Click);
            // 
            // btnLPmuon
            // 
            this.btnLPmuon.Location = new System.Drawing.Point(0, 2);
            this.btnLPmuon.Name = "btnLPmuon";
            this.btnLPmuon.Size = new System.Drawing.Size(75, 23);
            this.btnLPmuon.TabIndex = 0;
            this.btnLPmuon.Text = "Lâp phiếu";
            this.btnLPmuon.UseVisualStyleBackColor = true;
            this.btnLPmuon.Click += new System.EventHandler(this.btnLPmuon_Click);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.pnlapphieutra);
            this.tabPage9.Controls.Add(this.pnXemPhieuTra);
            this.tabPage9.Controls.Add(this.btnXoaPhieuTra);
            this.tabPage9.Controls.Add(this.btnSuaPhieuTra);
            this.tabPage9.Controls.Add(this.btnCapNhatPhieuTra);
            this.tabPage9.Controls.Add(this.btnXemPhieuTra);
            this.tabPage9.Controls.Add(this.btnLpTra);
            this.tabPage9.Location = new System.Drawing.Point(4, 29);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(863, 430);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Phiếu trả";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // pnlapphieutra
            // 
            this.pnlapphieutra.Controls.Add(this.dateTimePicker2);
            this.pnlapphieutra.Controls.Add(this.label31);
            this.pnlapphieutra.Controls.Add(this.textBox3);
            this.pnlapphieutra.Controls.Add(this.label32);
            this.pnlapphieutra.Controls.Add(this.textBox4);
            this.pnlapphieutra.Controls.Add(this.label33);
            this.pnlapphieutra.Location = new System.Drawing.Point(0, 24);
            this.pnlapphieutra.Name = "pnlapphieutra";
            this.pnlapphieutra.Size = new System.Drawing.Size(864, 410);
            this.pnlapphieutra.TabIndex = 10;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(216, 263);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(58, 261);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(139, 23);
            this.label31.TabIndex = 7;
            this.label31.Text = "Ngày lập phiếu:";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(216, 167);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(194, 26);
            this.textBox3.TabIndex = 6;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(58, 167);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(105, 23);
            this.label32.TabIndex = 5;
            this.label32.Text = "Mã độc giả:";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(216, 80);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(194, 26);
            this.textBox4.TabIndex = 4;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(58, 80);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(93, 23);
            this.label33.TabIndex = 0;
            this.label33.Text = "Mã phiếu:";
            // 
            // pnXemPhieuTra
            // 
            this.pnXemPhieuTra.Controls.Add(this.dgvXemPhieuTra);
            this.pnXemPhieuTra.Location = new System.Drawing.Point(1, 24);
            this.pnXemPhieuTra.Name = "pnXemPhieuTra";
            this.pnXemPhieuTra.Size = new System.Drawing.Size(860, 410);
            this.pnXemPhieuTra.TabIndex = 11;
            // 
            // dgvXemPhieuTra
            // 
            this.dgvXemPhieuTra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvXemPhieuTra.Location = new System.Drawing.Point(0, 0);
            this.dgvXemPhieuTra.Name = "dgvXemPhieuTra";
            this.dgvXemPhieuTra.Size = new System.Drawing.Size(863, 410);
            this.dgvXemPhieuTra.TabIndex = 0;
            // 
            // btnXoaPhieuTra
            // 
            this.btnXoaPhieuTra.Location = new System.Drawing.Point(305, 2);
            this.btnXoaPhieuTra.Name = "btnXoaPhieuTra";
            this.btnXoaPhieuTra.Size = new System.Drawing.Size(75, 23);
            this.btnXoaPhieuTra.TabIndex = 9;
            this.btnXoaPhieuTra.Text = "Xóa";
            this.btnXoaPhieuTra.UseVisualStyleBackColor = true;
            // 
            // btnSuaPhieuTra
            // 
            this.btnSuaPhieuTra.Location = new System.Drawing.Point(233, 2);
            this.btnSuaPhieuTra.Name = "btnSuaPhieuTra";
            this.btnSuaPhieuTra.Size = new System.Drawing.Size(75, 23);
            this.btnSuaPhieuTra.TabIndex = 8;
            this.btnSuaPhieuTra.Text = "Sửa";
            this.btnSuaPhieuTra.UseVisualStyleBackColor = true;
            // 
            // btnCapNhatPhieuTra
            // 
            this.btnCapNhatPhieuTra.Location = new System.Drawing.Point(161, 2);
            this.btnCapNhatPhieuTra.Name = "btnCapNhatPhieuTra";
            this.btnCapNhatPhieuTra.Size = new System.Drawing.Size(75, 23);
            this.btnCapNhatPhieuTra.TabIndex = 7;
            this.btnCapNhatPhieuTra.Text = "Cập nhật";
            this.btnCapNhatPhieuTra.UseVisualStyleBackColor = true;
            // 
            // btnXemPhieuTra
            // 
            this.btnXemPhieuTra.Location = new System.Drawing.Point(72, 2);
            this.btnXemPhieuTra.Name = "btnXemPhieuTra";
            this.btnXemPhieuTra.Size = new System.Drawing.Size(91, 23);
            this.btnXemPhieuTra.TabIndex = 6;
            this.btnXemPhieuTra.Text = "Xem danh sách";
            this.btnXemPhieuTra.UseVisualStyleBackColor = true;
            this.btnXemPhieuTra.Click += new System.EventHandler(this.btnXemPhieuTra_Click);
            // 
            // btnLpTra
            // 
            this.btnLpTra.Location = new System.Drawing.Point(0, 2);
            this.btnLpTra.Name = "btnLpTra";
            this.btnLpTra.Size = new System.Drawing.Size(75, 23);
            this.btnLpTra.TabIndex = 5;
            this.btnLpTra.Text = "Lâp phiếu";
            this.btnLpTra.UseVisualStyleBackColor = true;
            this.btnLpTra.Click += new System.EventHandler(this.btnLpTra_Click);
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.pnLapPhieuNhacNho);
            this.tabPage10.Controls.Add(this.pnXemPhieuNhacNho);
            this.tabPage10.Controls.Add(this.btnXoaPhieuNhacNho);
            this.tabPage10.Controls.Add(this.btnSuaPhieuNhacNho);
            this.tabPage10.Controls.Add(this.btnCapNhatPhieuNhacNho);
            this.tabPage10.Controls.Add(this.btnXemPNhacNho);
            this.tabPage10.Controls.Add(this.btnLPNhacNho);
            this.tabPage10.Location = new System.Drawing.Point(4, 29);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(863, 430);
            this.tabPage10.TabIndex = 2;
            this.tabPage10.Text = "Phiếu nhắc nhở";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // pnLapPhieuNhacNho
            // 
            this.pnLapPhieuNhacNho.Controls.Add(this.dateTimePicker3);
            this.pnLapPhieuNhacNho.Controls.Add(this.label34);
            this.pnLapPhieuNhacNho.Controls.Add(this.textBox5);
            this.pnLapPhieuNhacNho.Controls.Add(this.label35);
            this.pnLapPhieuNhacNho.Controls.Add(this.textBox6);
            this.pnLapPhieuNhacNho.Controls.Add(this.label36);
            this.pnLapPhieuNhacNho.Location = new System.Drawing.Point(0, 24);
            this.pnLapPhieuNhacNho.Name = "pnLapPhieuNhacNho";
            this.pnLapPhieuNhacNho.Size = new System.Drawing.Size(860, 410);
            this.pnLapPhieuNhacNho.TabIndex = 10;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(216, 263);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker3.TabIndex = 8;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(58, 261);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(139, 23);
            this.label34.TabIndex = 7;
            this.label34.Text = "Ngày lập phiếu:";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(216, 167);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(194, 26);
            this.textBox5.TabIndex = 6;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(58, 167);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(105, 23);
            this.label35.TabIndex = 5;
            this.label35.Text = "Mã độc giả:";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(216, 80);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(194, 26);
            this.textBox6.TabIndex = 4;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(58, 80);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(93, 23);
            this.label36.TabIndex = 0;
            this.label36.Text = "Mã phiếu:";
            // 
            // pnXemPhieuNhacNho
            // 
            this.pnXemPhieuNhacNho.Controls.Add(this.dgvXemPhieuNhacNho);
            this.pnXemPhieuNhacNho.Location = new System.Drawing.Point(0, 26);
            this.pnXemPhieuNhacNho.Name = "pnXemPhieuNhacNho";
            this.pnXemPhieuNhacNho.Size = new System.Drawing.Size(860, 410);
            this.pnXemPhieuNhacNho.TabIndex = 12;
            // 
            // dgvXemPhieuNhacNho
            // 
            this.dgvXemPhieuNhacNho.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvXemPhieuNhacNho.Location = new System.Drawing.Point(0, 0);
            this.dgvXemPhieuNhacNho.Name = "dgvXemPhieuNhacNho";
            this.dgvXemPhieuNhacNho.Size = new System.Drawing.Size(500, 410);
            this.dgvXemPhieuNhacNho.TabIndex = 0;
            // 
            // btnXoaPhieuNhacNho
            // 
            this.btnXoaPhieuNhacNho.Location = new System.Drawing.Point(305, 2);
            this.btnXoaPhieuNhacNho.Name = "btnXoaPhieuNhacNho";
            this.btnXoaPhieuNhacNho.Size = new System.Drawing.Size(75, 23);
            this.btnXoaPhieuNhacNho.TabIndex = 9;
            this.btnXoaPhieuNhacNho.Text = "Xóa";
            this.btnXoaPhieuNhacNho.UseVisualStyleBackColor = true;
            // 
            // btnSuaPhieuNhacNho
            // 
            this.btnSuaPhieuNhacNho.Location = new System.Drawing.Point(233, 2);
            this.btnSuaPhieuNhacNho.Name = "btnSuaPhieuNhacNho";
            this.btnSuaPhieuNhacNho.Size = new System.Drawing.Size(75, 23);
            this.btnSuaPhieuNhacNho.TabIndex = 8;
            this.btnSuaPhieuNhacNho.Text = "Sửa";
            this.btnSuaPhieuNhacNho.UseVisualStyleBackColor = true;
            // 
            // btnCapNhatPhieuNhacNho
            // 
            this.btnCapNhatPhieuNhacNho.Location = new System.Drawing.Point(161, 2);
            this.btnCapNhatPhieuNhacNho.Name = "btnCapNhatPhieuNhacNho";
            this.btnCapNhatPhieuNhacNho.Size = new System.Drawing.Size(75, 23);
            this.btnCapNhatPhieuNhacNho.TabIndex = 7;
            this.btnCapNhatPhieuNhacNho.Text = "Cập nhật";
            this.btnCapNhatPhieuNhacNho.UseVisualStyleBackColor = true;
            // 
            // btnXemPNhacNho
            // 
            this.btnXemPNhacNho.Location = new System.Drawing.Point(72, 2);
            this.btnXemPNhacNho.Name = "btnXemPNhacNho";
            this.btnXemPNhacNho.Size = new System.Drawing.Size(91, 23);
            this.btnXemPNhacNho.TabIndex = 6;
            this.btnXemPNhacNho.Text = "Xem danh sách";
            this.btnXemPNhacNho.UseVisualStyleBackColor = true;
            this.btnXemPNhacNho.Click += new System.EventHandler(this.btnXemPNhacNho_Click);
            // 
            // btnLPNhacNho
            // 
            this.btnLPNhacNho.Location = new System.Drawing.Point(0, 2);
            this.btnLPNhacNho.Name = "btnLPNhacNho";
            this.btnLPNhacNho.Size = new System.Drawing.Size(75, 23);
            this.btnLPNhacNho.TabIndex = 5;
            this.btnLPNhacNho.Text = "Lâp phiếu";
            this.btnLPNhacNho.UseVisualStyleBackColor = true;
            this.btnLPNhacNho.Click += new System.EventHandler(this.btnLPNhacNho_Click);
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.pnLapPhieuPhat);
            this.tabPage11.Controls.Add(this.pnXemPhieuPhat);
            this.tabPage11.Controls.Add(this.button20);
            this.tabPage11.Controls.Add(this.button21);
            this.tabPage11.Controls.Add(this.button22);
            this.tabPage11.Controls.Add(this.btnXemPhieuPhat);
            this.tabPage11.Controls.Add(this.btnLapPhieuPhat);
            this.tabPage11.Location = new System.Drawing.Point(4, 29);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(863, 430);
            this.tabPage11.TabIndex = 3;
            this.tabPage11.Text = "Phiếu phạt";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // pnLapPhieuPhat
            // 
            this.pnLapPhieuPhat.Controls.Add(this.dateTimePicker4);
            this.pnLapPhieuPhat.Controls.Add(this.label37);
            this.pnLapPhieuPhat.Controls.Add(this.textBox7);
            this.pnLapPhieuPhat.Controls.Add(this.label38);
            this.pnLapPhieuPhat.Controls.Add(this.textBox8);
            this.pnLapPhieuPhat.Controls.Add(this.label39);
            this.pnLapPhieuPhat.Location = new System.Drawing.Point(0, 24);
            this.pnLapPhieuPhat.Name = "pnLapPhieuPhat";
            this.pnLapPhieuPhat.Size = new System.Drawing.Size(860, 410);
            this.pnLapPhieuPhat.TabIndex = 10;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(216, 263);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker4.TabIndex = 8;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(58, 261);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(139, 23);
            this.label37.TabIndex = 7;
            this.label37.Text = "Ngày lập phiếu:";
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(216, 167);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(194, 26);
            this.textBox7.TabIndex = 6;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(58, 167);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(105, 23);
            this.label38.TabIndex = 5;
            this.label38.Text = "Mã độc giả:";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(216, 80);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(194, 26);
            this.textBox8.TabIndex = 4;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(58, 80);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(93, 23);
            this.label39.TabIndex = 0;
            this.label39.Text = "Mã phiếu:";
            // 
            // pnXemPhieuPhat
            // 
            this.pnXemPhieuPhat.Controls.Add(this.dgvXemPhieuPhat);
            this.pnXemPhieuPhat.Location = new System.Drawing.Point(1, 25);
            this.pnXemPhieuPhat.Name = "pnXemPhieuPhat";
            this.pnXemPhieuPhat.Size = new System.Drawing.Size(860, 410);
            this.pnXemPhieuPhat.TabIndex = 11;
            // 
            // dgvXemPhieuPhat
            // 
            this.dgvXemPhieuPhat.ColumnHeadersHeight = 22;
            this.dgvXemPhieuPhat.Location = new System.Drawing.Point(0, 0);
            this.dgvXemPhieuPhat.Name = "dgvXemPhieuPhat";
            this.dgvXemPhieuPhat.Size = new System.Drawing.Size(500, 410);
            this.dgvXemPhieuPhat.TabIndex = 0;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(305, 2);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 9;
            this.button20.Text = "Xóa";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(233, 2);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 8;
            this.button21.Text = "Sửa";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(161, 2);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 7;
            this.button22.Text = "Cập nhật";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // btnXemPhieuPhat
            // 
            this.btnXemPhieuPhat.Location = new System.Drawing.Point(72, 2);
            this.btnXemPhieuPhat.Name = "btnXemPhieuPhat";
            this.btnXemPhieuPhat.Size = new System.Drawing.Size(91, 23);
            this.btnXemPhieuPhat.TabIndex = 6;
            this.btnXemPhieuPhat.Text = "Xem danh sách";
            this.btnXemPhieuPhat.UseVisualStyleBackColor = true;
            this.btnXemPhieuPhat.Click += new System.EventHandler(this.btnXemPhieuPhat_Click);
            // 
            // btnLapPhieuPhat
            // 
            this.btnLapPhieuPhat.Location = new System.Drawing.Point(0, 2);
            this.btnLapPhieuPhat.Name = "btnLapPhieuPhat";
            this.btnLapPhieuPhat.Size = new System.Drawing.Size(75, 23);
            this.btnLapPhieuPhat.TabIndex = 5;
            this.btnLapPhieuPhat.Text = "Lâp phiếu";
            this.btnLapPhieuPhat.UseVisualStyleBackColor = true;
            this.btnLapPhieuPhat.Click += new System.EventHandler(this.btnLapPhieuPhat_Click);
            // 
            // lbTimkiemDG
            // 
            this.lbTimkiemDG.BackColor = System.Drawing.Color.Transparent;
            this.lbTimkiemDG.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbTimkiemDG.ForeColor = System.Drawing.Color.LimeGreen;
            this.lbTimkiemDG.Location = new System.Drawing.Point(147, 29);
            this.lbTimkiemDG.Name = "lbTimkiemDG";
            this.lbTimkiemDG.Size = new System.Drawing.Size(187, 23);
            this.lbTimkiemDG.TabIndex = 14;
            this.lbTimkiemDG.Text = "Tìm Kiếm Đọc Giả";
            this.lbTimkiemDG.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTimKiemSach
            // 
            this.lbTimKiemSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbTimKiemSach.ForeColor = System.Drawing.Color.LimeGreen;
            this.lbTimKiemSach.Location = new System.Drawing.Point(606, 29);
            this.lbTimKiemSach.Name = "lbTimKiemSach";
            this.lbTimKiemSach.Size = new System.Drawing.Size(165, 23);
            this.lbTimKiemSach.TabIndex = 15;
            this.lbTimKiemSach.Text = "Tìm Kiếm Sách";
            this.lbTimKiemSach.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label23.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label23.Image = ((System.Drawing.Image)(resources.GetObject("label23.Image")));
            this.label23.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label23.Location = new System.Drawing.Point(58, 73);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Padding = new System.Windows.Forms.Padding(2);
            this.label23.Size = new System.Drawing.Size(149, 54);
            this.label23.TabIndex = 42;
            this.label23.Text = "Tên Đọc Giả :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label23.UseCompatibleTextRendering = true;
            // 
            // textBox21
            // 
            this.textBox21.AllowDrop = true;
            this.textBox21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.textBox21.Location = new System.Drawing.Point(210, 79);
            this.textBox21.Multiline = true;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(212, 31);
            this.textBox21.TabIndex = 41;
            // 
            // label24
            // 
            this.label24.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label24.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label24.Image = ((System.Drawing.Image)(resources.GetObject("label24.Image")));
            this.label24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label24.Location = new System.Drawing.Point(58, 124);
            this.label24.Margin = new System.Windows.Forms.Padding(0);
            this.label24.Name = "label24";
            this.label24.Padding = new System.Windows.Forms.Padding(2);
            this.label24.Size = new System.Drawing.Size(115, 54);
            this.label24.TabIndex = 49;
            this.label24.Text = "CMND :";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label24.UseCompatibleTextRendering = true;
            // 
            // textBox22
            // 
            this.textBox22.AllowDrop = true;
            this.textBox22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.textBox22.Location = new System.Drawing.Point(210, 136);
            this.textBox22.Multiline = true;
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(212, 31);
            this.textBox22.TabIndex = 50;
            // 
            // label26
            // 
            this.label26.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label26.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label26.Image = ((System.Drawing.Image)(resources.GetObject("label26.Image")));
            this.label26.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label26.Location = new System.Drawing.Point(488, 76);
            this.label26.Margin = new System.Windows.Forms.Padding(0);
            this.label26.Name = "label26";
            this.label26.Padding = new System.Windows.Forms.Padding(2);
            this.label26.Size = new System.Drawing.Size(172, 51);
            this.label26.TabIndex = 52;
            this.label26.Text = "Mã Loại Sách :";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label26.UseCompatibleTextRendering = true;
            // 
            // textBox24
            // 
            this.textBox24.AllowDrop = true;
            this.textBox24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.textBox24.Location = new System.Drawing.Point(658, 79);
            this.textBox24.Multiline = true;
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(212, 31);
            this.textBox24.TabIndex = 51;
            // 
            // label25
            // 
            this.label25.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label25.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label25.Image = ((System.Drawing.Image)(resources.GetObject("label25.Image")));
            this.label25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label25.Location = new System.Drawing.Point(488, 131);
            this.label25.Margin = new System.Windows.Forms.Padding(0);
            this.label25.Name = "label25";
            this.label25.Padding = new System.Windows.Forms.Padding(2);
            this.label25.Size = new System.Drawing.Size(172, 47);
            this.label25.TabIndex = 53;
            this.label25.Text = "Tên Loại Sách :";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label25.UseCompatibleTextRendering = true;
            // 
            // textBox23
            // 
            this.textBox23.AllowDrop = true;
            this.textBox23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.textBox23.Location = new System.Drawing.Point(658, 136);
            this.textBox23.Multiline = true;
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(212, 31);
            this.textBox23.TabIndex = 54;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.SeaGreen;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button9.Location = new System.Drawing.Point(58, 200);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(128, 38);
            this.button9.TabIndex = 55;
            this.button9.Text = "Theo Tên";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.SeaGreen;
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button10.Location = new System.Drawing.Point(283, 200);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(145, 38);
            this.button10.TabIndex = 56;
            this.button10.Text = "Theo CMND";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.SeaGreen;
            this.button12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button12.Location = new System.Drawing.Point(506, 200);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(128, 38);
            this.button12.TabIndex = 60;
            this.button12.Text = "Theo Mã Sách";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.SeaGreen;
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button11.Location = new System.Drawing.Point(731, 200);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(145, 38);
            this.button11.TabIndex = 61;
            this.button11.Text = "Theo Tên Sách";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Orange;
            this.button13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(151, 339);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(183, 38);
            this.button13.TabIndex = 63;
            this.button13.Text = "Tìm Kiếm";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Orange;
            this.button14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Image = ((System.Drawing.Image)(resources.GetObject("button14.Image")));
            this.button14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.Location = new System.Drawing.Point(630, 339);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(183, 38);
            this.button14.TabIndex = 64;
            this.button14.Text = "Tìm Kiếm";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // panelTraCuu
            // 
            this.panelTraCuu.Controls.Add(this.button14);
            this.panelTraCuu.Controls.Add(this.button13);
            this.panelTraCuu.Controls.Add(this.button11);
            this.panelTraCuu.Controls.Add(this.button12);
            this.panelTraCuu.Controls.Add(this.button10);
            this.panelTraCuu.Controls.Add(this.button9);
            this.panelTraCuu.Controls.Add(this.textBox23);
            this.panelTraCuu.Controls.Add(this.label25);
            this.panelTraCuu.Controls.Add(this.textBox24);
            this.panelTraCuu.Controls.Add(this.label26);
            this.panelTraCuu.Controls.Add(this.textBox22);
            this.panelTraCuu.Controls.Add(this.label24);
            this.panelTraCuu.Controls.Add(this.textBox21);
            this.panelTraCuu.Controls.Add(this.label23);
            this.panelTraCuu.Controls.Add(this.lbTimKiemSach);
            this.panelTraCuu.Controls.Add(this.lbTimkiemDG);
            this.panelTraCuu.Location = new System.Drawing.Point(69, 279);
            this.panelTraCuu.Name = "panelTraCuu";
            this.panelTraCuu.Size = new System.Drawing.Size(953, 466);
            this.panelTraCuu.TabIndex = 14;
            // 
            // tabPhieuPhat
            // 
            this.tabPhieuPhat.Controls.Add(this.comboBox3);
            this.tabPhieuPhat.Controls.Add(this.button30);
            this.tabPhieuPhat.Controls.Add(this.button31);
            this.tabPhieuPhat.Controls.Add(this.button32);
            this.tabPhieuPhat.Controls.Add(this.button33);
            this.tabPhieuPhat.Controls.Add(this.button34);
            this.tabPhieuPhat.Controls.Add(this.button35);
            this.tabPhieuPhat.Controls.Add(this.button36);
            this.tabPhieuPhat.Controls.Add(this.button37);
            this.tabPhieuPhat.Controls.Add(this.dataGridView3);
            this.tabPhieuPhat.Controls.Add(this.button38);
            this.tabPhieuPhat.Controls.Add(this.label50);
            this.tabPhieuPhat.Controls.Add(this.radioButton7);
            this.tabPhieuPhat.Controls.Add(this.radioButton8);
            this.tabPhieuPhat.Controls.Add(this.radioButton9);
            this.tabPhieuPhat.Controls.Add(this.textBox12);
            this.tabPhieuPhat.Controls.Add(this.label51);
            this.tabPhieuPhat.Location = new System.Drawing.Point(4, 29);
            this.tabPhieuPhat.Name = "tabPhieuPhat";
            this.tabPhieuPhat.Size = new System.Drawing.Size(863, 430);
            this.tabPhieuPhat.TabIndex = 6;
            this.tabPhieuPhat.Text = "LàmLai Phiếu Phạt";
            this.tabPhieuPhat.UseVisualStyleBackColor = true;
            // 
            // tabPhieuNhacNho
            // 
            this.tabPhieuNhacNho.Controls.Add(this.comboBox4);
            this.tabPhieuNhacNho.Controls.Add(this.button39);
            this.tabPhieuNhacNho.Controls.Add(this.button40);
            this.tabPhieuNhacNho.Controls.Add(this.button41);
            this.tabPhieuNhacNho.Controls.Add(this.button42);
            this.tabPhieuNhacNho.Controls.Add(this.button43);
            this.tabPhieuNhacNho.Controls.Add(this.button44);
            this.tabPhieuNhacNho.Controls.Add(this.button45);
            this.tabPhieuNhacNho.Controls.Add(this.button46);
            this.tabPhieuNhacNho.Controls.Add(this.dataGridView4);
            this.tabPhieuNhacNho.Controls.Add(this.button47);
            this.tabPhieuNhacNho.Controls.Add(this.label52);
            this.tabPhieuNhacNho.Controls.Add(this.radioButton10);
            this.tabPhieuNhacNho.Controls.Add(this.radioButton11);
            this.tabPhieuNhacNho.Controls.Add(this.radioButton12);
            this.tabPhieuNhacNho.Controls.Add(this.textBox13);
            this.tabPhieuNhacNho.Controls.Add(this.label53);
            this.tabPhieuNhacNho.Location = new System.Drawing.Point(4, 29);
            this.tabPhieuNhacNho.Name = "tabPhieuNhacNho";
            this.tabPhieuNhacNho.Size = new System.Drawing.Size(863, 430);
            this.tabPhieuNhacNho.TabIndex = 7;
            this.tabPhieuNhacNho.Text = "Làm Lại Phiếu Nhắc Nhở";
            this.tabPhieuNhacNho.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(408, 68);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 39;
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button30.ForeColor = System.Drawing.Color.White;
            this.button30.Location = new System.Drawing.Point(111, 354);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(138, 43);
            this.button30.TabIndex = 38;
            this.button30.Text = "Chỉnh Sửa";
            this.button30.UseVisualStyleBackColor = false;
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button31.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button31.Location = new System.Drawing.Point(544, 353);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(133, 44);
            this.button31.TabIndex = 37;
            this.button31.Text = "Hủy";
            this.button31.UseVisualStyleBackColor = false;
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button32.ForeColor = System.Drawing.Color.White;
            this.button32.Location = new System.Drawing.Point(319, 354);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(138, 43);
            this.button32.TabIndex = 36;
            this.button32.Text = "Lưu";
            this.button32.UseVisualStyleBackColor = false;
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.Color.Purple;
            this.button33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button33.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button33.Location = new System.Drawing.Point(703, 72);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(116, 33);
            this.button33.TabIndex = 35;
            this.button33.Text = "Xem Tất Cả";
            this.button33.UseVisualStyleBackColor = false;
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.Color.Green;
            this.button34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button34.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button34.Location = new System.Drawing.Point(663, 354);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(176, 41);
            this.button34.TabIndex = 34;
            this.button34.Text = "Lập Phiếu Cảnh Cáo";
            this.button34.UseVisualStyleBackColor = false;
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.Color.Green;
            this.button35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button35.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button35.Location = new System.Drawing.Point(497, 354);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(129, 41);
            this.button35.TabIndex = 33;
            this.button35.Text = "Lập Phiếu Trả";
            this.button35.UseVisualStyleBackColor = false;
            // 
            // button36
            // 
            this.button36.BackColor = System.Drawing.Color.Green;
            this.button36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button36.ForeColor = System.Drawing.Color.White;
            this.button36.Location = new System.Drawing.Point(171, 354);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(117, 41);
            this.button36.TabIndex = 32;
            this.button36.Text = "Xóa";
            this.button36.UseVisualStyleBackColor = false;
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.Green;
            this.button37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button37.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button37.Location = new System.Drawing.Point(21, 354);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(117, 41);
            this.button37.TabIndex = 31;
            this.button37.Text = "Xem Chi Tiết";
            this.button37.UseVisualStyleBackColor = false;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(11, 131);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(840, 199);
            this.dataGridView3.TabIndex = 30;
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.Color.Purple;
            this.button38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button38.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button38.Location = new System.Drawing.Point(554, 72);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(123, 33);
            this.button38.TabIndex = 29;
            this.button38.Text = "Tìm Kiếm";
            this.button38.UseVisualStyleBackColor = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(562, 36);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(115, 13);
            this.label50.TabIndex = 28;
            this.label50.Text = "CMND/MSSV/MSCB :";
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(275, 108);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(61, 17);
            this.radioButton7.TabIndex = 27;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Họ Tên";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(275, 72);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(127, 17);
            this.radioButton8.TabIndex = 26;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "CMND/MSSV/MSCB";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(275, 36);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(82, 17);
            this.radioButton9.TabIndex = 25;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "Mã Độc Giả";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(687, 33);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(122, 20);
            this.textBox12.TabIndex = 24;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label51.Location = new System.Drawing.Point(81, 50);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(168, 25);
            this.label51.TabIndex = 23;
            this.label51.Text = "Tìm Kiếm Theo :";
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(408, 68);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 21);
            this.comboBox4.TabIndex = 39;
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button39.ForeColor = System.Drawing.Color.White;
            this.button39.Location = new System.Drawing.Point(111, 354);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(138, 43);
            this.button39.TabIndex = 38;
            this.button39.Text = "Chỉnh Sửa";
            this.button39.UseVisualStyleBackColor = false;
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button40.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button40.Location = new System.Drawing.Point(544, 353);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(133, 44);
            this.button40.TabIndex = 37;
            this.button40.Text = "Hủy";
            this.button40.UseVisualStyleBackColor = false;
            // 
            // button41
            // 
            this.button41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button41.ForeColor = System.Drawing.Color.White;
            this.button41.Location = new System.Drawing.Point(319, 354);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(138, 43);
            this.button41.TabIndex = 36;
            this.button41.Text = "Lưu";
            this.button41.UseVisualStyleBackColor = false;
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.Color.Purple;
            this.button42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button42.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button42.Location = new System.Drawing.Point(703, 72);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(116, 33);
            this.button42.TabIndex = 35;
            this.button42.Text = "Xem Tất Cả";
            this.button42.UseVisualStyleBackColor = false;
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.Green;
            this.button43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button43.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button43.Location = new System.Drawing.Point(663, 354);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(176, 41);
            this.button43.TabIndex = 34;
            this.button43.Text = "Lập Phiếu Cảnh Cáo";
            this.button43.UseVisualStyleBackColor = false;
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.Green;
            this.button44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button44.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button44.Location = new System.Drawing.Point(497, 354);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(129, 41);
            this.button44.TabIndex = 33;
            this.button44.Text = "Lập Phiếu Trả";
            this.button44.UseVisualStyleBackColor = false;
            // 
            // button45
            // 
            this.button45.BackColor = System.Drawing.Color.Green;
            this.button45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button45.ForeColor = System.Drawing.Color.White;
            this.button45.Location = new System.Drawing.Point(171, 354);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(117, 41);
            this.button45.TabIndex = 32;
            this.button45.Text = "Xóa";
            this.button45.UseVisualStyleBackColor = false;
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.Color.Green;
            this.button46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button46.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button46.Location = new System.Drawing.Point(21, 354);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(117, 41);
            this.button46.TabIndex = 31;
            this.button46.Text = "Xem Chi Tiết";
            this.button46.UseVisualStyleBackColor = false;
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(11, 131);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(840, 199);
            this.dataGridView4.TabIndex = 30;
            // 
            // button47
            // 
            this.button47.BackColor = System.Drawing.Color.Purple;
            this.button47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button47.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button47.Location = new System.Drawing.Point(554, 72);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(123, 33);
            this.button47.TabIndex = 29;
            this.button47.Text = "Tìm Kiếm";
            this.button47.UseVisualStyleBackColor = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(562, 36);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(115, 13);
            this.label52.TabIndex = 28;
            this.label52.Text = "CMND/MSSV/MSCB :";
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(275, 108);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(61, 17);
            this.radioButton10.TabIndex = 27;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "Họ Tên";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(275, 72);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(127, 17);
            this.radioButton11.TabIndex = 26;
            this.radioButton11.TabStop = true;
            this.radioButton11.Text = "CMND/MSSV/MSCB";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Location = new System.Drawing.Point(275, 36);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(82, 17);
            this.radioButton12.TabIndex = 25;
            this.radioButton12.TabStop = true;
            this.radioButton12.Text = "Mã Độc Giả";
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(687, 33);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(122, 20);
            this.textBox13.TabIndex = 24;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label53.Location = new System.Drawing.Point(81, 50);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(168, 25);
            this.label53.TabIndex = 23;
            this.label53.Text = "Tìm Kiếm Theo :";
            // 
            // tabPhieuMuon
            // 
            this.tabPhieuMuon.Controls.Add(this.tabControl2);
            this.tabPhieuMuon.Location = new System.Drawing.Point(4, 29);
            this.tabPhieuMuon.Name = "tabPhieuMuon";
            this.tabPhieuMuon.Size = new System.Drawing.Size(863, 430);
            this.tabPhieuMuon.TabIndex = 4;
            this.tabPhieuMuon.Text = "Làm Lại Phiếu Mượn";
            this.tabPhieuMuon.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabThemPhieuMuon);
            this.tabControl2.Controls.Add(this.tabTimKiemPhieuMuon);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(863, 430);
            this.tabControl2.TabIndex = 0;
            // 
            // tabThemPhieuMuon
            // 
            this.tabThemPhieuMuon.Controls.Add(this.button48);
            this.tabThemPhieuMuon.Controls.Add(this.textBox14);
            this.tabThemPhieuMuon.Controls.Add(this.label54);
            this.tabThemPhieuMuon.Controls.Add(this.dateTimePicker1);
            this.tabThemPhieuMuon.Controls.Add(this.label55);
            this.tabThemPhieuMuon.Controls.Add(this.textBox15);
            this.tabThemPhieuMuon.Controls.Add(this.label56);
            this.tabThemPhieuMuon.Controls.Add(this.textBox16);
            this.tabThemPhieuMuon.Controls.Add(this.label57);
            this.tabThemPhieuMuon.Controls.Add(this.dateTimePicker5);
            this.tabThemPhieuMuon.Controls.Add(this.label58);
            this.tabThemPhieuMuon.Controls.Add(this.textBox17);
            this.tabThemPhieuMuon.Controls.Add(this.label59);
            this.tabThemPhieuMuon.Controls.Add(this.textBox18);
            this.tabThemPhieuMuon.Controls.Add(this.label60);
            this.tabThemPhieuMuon.Location = new System.Drawing.Point(4, 22);
            this.tabThemPhieuMuon.Name = "tabThemPhieuMuon";
            this.tabThemPhieuMuon.Padding = new System.Windows.Forms.Padding(3);
            this.tabThemPhieuMuon.Size = new System.Drawing.Size(855, 404);
            this.tabThemPhieuMuon.TabIndex = 0;
            this.tabThemPhieuMuon.Text = "Thêm Phiếu Mượn";
            this.tabThemPhieuMuon.UseVisualStyleBackColor = true;
            // 
            // tabTimKiemPhieuMuon
            // 
            this.tabTimKiemPhieuMuon.Controls.Add(this.comboBox1);
            this.tabTimKiemPhieuMuon.Controls.Add(this.button2);
            this.tabTimKiemPhieuMuon.Controls.Add(this.button4);
            this.tabTimKiemPhieuMuon.Controls.Add(this.button5);
            this.tabTimKiemPhieuMuon.Controls.Add(this.button6);
            this.tabTimKiemPhieuMuon.Controls.Add(this.button8);
            this.tabTimKiemPhieuMuon.Controls.Add(this.button15);
            this.tabTimKiemPhieuMuon.Controls.Add(this.button16);
            this.tabTimKiemPhieuMuon.Controls.Add(this.dataGridView1);
            this.tabTimKiemPhieuMuon.Controls.Add(this.button17);
            this.tabTimKiemPhieuMuon.Controls.Add(this.radioButton2);
            this.tabTimKiemPhieuMuon.Controls.Add(this.radioButton3);
            this.tabTimKiemPhieuMuon.Controls.Add(this.textBox9);
            this.tabTimKiemPhieuMuon.Controls.Add(this.label47);
            this.tabTimKiemPhieuMuon.Location = new System.Drawing.Point(4, 22);
            this.tabTimKiemPhieuMuon.Name = "tabTimKiemPhieuMuon";
            this.tabTimKiemPhieuMuon.Padding = new System.Windows.Forms.Padding(3);
            this.tabTimKiemPhieuMuon.Size = new System.Drawing.Size(855, 404);
            this.tabTimKiemPhieuMuon.TabIndex = 1;
            this.tabTimKiemPhieuMuon.Text = "Tìm Kiếm Phiếu Mượn";
            this.tabTimKiemPhieuMuon.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Quá Hạn",
            "Chưa Trả"});
            this.comboBox1.Location = new System.Drawing.Point(675, 81);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 56;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(75, 339);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 43);
            this.button2.TabIndex = 55;
            this.button2.Text = "Chỉnh Sửa";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button4.Location = new System.Drawing.Point(672, 341);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(94, 44);
            this.button4.TabIndex = 54;
            this.button4.Text = "Hủy";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(315, 341);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(138, 43);
            this.button5.TabIndex = 53;
            this.button5.Text = "Lưu";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Purple;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button6.Location = new System.Drawing.Point(672, 37);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(116, 33);
            this.button6.TabIndex = 52;
            this.button6.Text = "Xem Tất Cả";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Green;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button8.Location = new System.Drawing.Point(493, 341);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(129, 41);
            this.button8.TabIndex = 50;
            this.button8.Text = "Lập Phiếu Trả";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Green;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.Location = new System.Drawing.Point(167, 341);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(117, 41);
            this.button15.TabIndex = 49;
            this.button15.Text = "Xóa";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Green;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button16.Location = new System.Drawing.Point(17, 341);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(117, 41);
            this.button16.TabIndex = 48;
            this.button16.Text = "Xem Chi Tiết";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(7, 118);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(840, 199);
            this.dataGridView1.TabIndex = 47;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.Purple;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button17.Location = new System.Drawing.Point(535, 35);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(123, 33);
            this.button17.TabIndex = 46;
            this.button17.Text = "Tìm Kiếm";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(271, 59);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(100, 17);
            this.radioButton2.TabIndex = 43;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Mã Phiếu Mượn";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(271, 23);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(82, 17);
            this.radioButton3.TabIndex = 42;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Mã Độc Giả";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label47.Location = new System.Drawing.Point(77, 37);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(168, 25);
            this.label47.TabIndex = 40;
            this.label47.Text = "Tìm Kiếm Theo :";
            // 
            // button48
            // 
            this.button48.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(244)))));
            this.button48.Location = new System.Drawing.Point(415, 289);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(143, 36);
            this.button48.TabIndex = 32;
            this.button48.Text = "save";
            this.button48.UseVisualStyleBackColor = true;
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.Location = new System.Drawing.Point(727, 132);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(63, 26);
            this.textBox14.TabIndex = 31;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(689, 137);
            this.label54.Name = "label54";
            this.label54.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label54.Size = new System.Drawing.Size(32, 18);
            this.label54.TabIndex = 30;
            this.label54.Text = ",SL:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(590, 188);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker1.TabIndex = 29;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(500, 194);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(62, 18);
            this.label55.TabIndex = 28;
            this.label55.Text = "Hạn trả:";
            // 
            // textBox15
            // 
            this.textBox15.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(590, 132);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(86, 26);
            this.textBox15.TabIndex = 27;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(500, 137);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(81, 18);
            this.label56.TabIndex = 26;
            this.label56.Text = "Mã tài liệu:";
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.Location = new System.Drawing.Point(590, 79);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(200, 26);
            this.textBox16.TabIndex = 25;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(500, 84);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(84, 18);
            this.label57.TabIndex = 24;
            this.label57.Text = "STT mượn:";
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker5.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker5.Location = new System.Drawing.Point(214, 189);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker5.TabIndex = 23;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(64, 189);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(109, 18);
            this.label58.TabIndex = 22;
            this.label58.Text = "Ngày lập phiếu:";
            // 
            // textBox17
            // 
            this.textBox17.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox17.Location = new System.Drawing.Point(214, 134);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(200, 26);
            this.textBox17.TabIndex = 21;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(64, 135);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(84, 18);
            this.label59.TabIndex = 20;
            this.label59.Text = "Mã độc giả:";
            // 
            // textBox18
            // 
            this.textBox18.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox18.Location = new System.Drawing.Point(214, 80);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(200, 26);
            this.textBox18.TabIndex = 19;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(64, 80);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(74, 18);
            this.label60.TabIndex = 18;
            this.label60.Text = "Mã phiếu:";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(399, 44);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(122, 20);
            this.textBox9.TabIndex = 41;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(687, 33);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(122, 20);
            this.textBox10.TabIndex = 24;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(275, 36);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(82, 17);
            this.radioButton6.TabIndex = 25;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Mã Độc Giả";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(275, 72);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(127, 17);
            this.radioButton5.TabIndex = 26;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "CMND/MSSV/MSCB";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(275, 108);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(61, 17);
            this.radioButton4.TabIndex = 27;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Họ Tên";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(562, 36);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(115, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "CMND/MSSV/MSCB :";
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.Purple;
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button29.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button29.Location = new System.Drawing.Point(554, 72);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(123, 33);
            this.button29.TabIndex = 29;
            this.button29.Text = "Tìm Kiếm";
            this.button29.UseVisualStyleBackColor = false;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(11, 131);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(840, 199);
            this.dataGridView2.TabIndex = 30;
            // 
            // button28
            // 
            this.button28.BackColor = System.Drawing.Color.Green;
            this.button28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button28.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button28.Location = new System.Drawing.Point(21, 354);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(117, 41);
            this.button28.TabIndex = 31;
            this.button28.Text = "Xem Chi Tiết";
            this.button28.UseVisualStyleBackColor = false;
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.Green;
            this.button27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button27.ForeColor = System.Drawing.Color.White;
            this.button27.Location = new System.Drawing.Point(171, 354);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(117, 41);
            this.button27.TabIndex = 32;
            this.button27.Text = "Xóa";
            this.button27.UseVisualStyleBackColor = false;
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.Green;
            this.button26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button26.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button26.Location = new System.Drawing.Point(497, 354);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(129, 41);
            this.button26.TabIndex = 33;
            this.button26.Text = "Lập Phiếu Trả";
            this.button26.UseVisualStyleBackColor = false;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.Green;
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button25.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button25.Location = new System.Drawing.Point(663, 354);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(176, 41);
            this.button25.TabIndex = 34;
            this.button25.Text = "Lập Phiếu Cảnh Cáo";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.Purple;
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button24.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button24.Location = new System.Drawing.Point(703, 72);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(116, 33);
            this.button24.TabIndex = 35;
            this.button24.Text = "Xem Tất Cả";
            this.button24.UseVisualStyleBackColor = false;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button23.ForeColor = System.Drawing.Color.White;
            this.button23.Location = new System.Drawing.Point(319, 354);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(138, 43);
            this.button23.TabIndex = 36;
            this.button23.Text = "Lưu";
            this.button23.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button19.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button19.Location = new System.Drawing.Point(544, 353);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(133, 44);
            this.button19.TabIndex = 37;
            this.button19.Text = "Hủy";
            this.button19.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(111, 354);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(138, 43);
            this.button18.TabIndex = 38;
            this.button18.Text = "Chỉnh Sửa";
            this.button18.UseVisualStyleBackColor = false;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(408, 68);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 39;
            // 
            // tabPhieuTra
            // 
            this.tabPhieuTra.Controls.Add(this.tabControl3);
            this.tabPhieuTra.Controls.Add(this.comboBox2);
            this.tabPhieuTra.Controls.Add(this.button18);
            this.tabPhieuTra.Controls.Add(this.button19);
            this.tabPhieuTra.Controls.Add(this.button23);
            this.tabPhieuTra.Controls.Add(this.button24);
            this.tabPhieuTra.Controls.Add(this.button25);
            this.tabPhieuTra.Controls.Add(this.button26);
            this.tabPhieuTra.Controls.Add(this.button27);
            this.tabPhieuTra.Controls.Add(this.button28);
            this.tabPhieuTra.Controls.Add(this.dataGridView2);
            this.tabPhieuTra.Controls.Add(this.button29);
            this.tabPhieuTra.Controls.Add(this.label48);
            this.tabPhieuTra.Controls.Add(this.radioButton4);
            this.tabPhieuTra.Controls.Add(this.radioButton5);
            this.tabPhieuTra.Controls.Add(this.radioButton6);
            this.tabPhieuTra.Controls.Add(this.textBox10);
            this.tabPhieuTra.Controls.Add(this.label49);
            this.tabPhieuTra.Location = new System.Drawing.Point(4, 29);
            this.tabPhieuTra.Name = "tabPhieuTra";
            this.tabPhieuTra.Size = new System.Drawing.Size(863, 430);
            this.tabPhieuTra.TabIndex = 5;
            this.tabPhieuTra.Text = "Làm Lại Phiếu Trả";
            this.tabPhieuTra.UseVisualStyleBackColor = true;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label49.Location = new System.Drawing.Point(81, 50);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(168, 25);
            this.label49.TabIndex = 23;
            this.label49.Text = "Tìm Kiếm Theo :";
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage3);
            this.tabControl3.Controls.Add(this.tabPage12);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(0, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(863, 430);
            this.tabControl3.TabIndex = 40;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.textBox19);
            this.tabPage3.Controls.Add(this.label46);
            this.tabPage3.Controls.Add(this.dateTimePicker7);
            this.tabPage3.Controls.Add(this.label64);
            this.tabPage3.Controls.Add(this.textBox26);
            this.tabPage3.Controls.Add(this.label65);
            this.tabPage3.Controls.Add(this.textBox27);
            this.tabPage3.Controls.Add(this.label66);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(855, 404);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Thêm Phiếu Mượn";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(244)))));
            this.button7.Location = new System.Drawing.Point(227, 291);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(143, 36);
            this.button7.TabIndex = 32;
            this.button7.Text = "save";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // textBox19
            // 
            this.textBox19.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.Location = new System.Drawing.Point(727, 132);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(63, 26);
            this.textBox19.TabIndex = 31;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(689, 137);
            this.label46.Name = "label46";
            this.label46.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label46.Size = new System.Drawing.Size(32, 18);
            this.label46.TabIndex = 30;
            this.label46.Text = ",SL:";
            // 
            // dateTimePicker7
            // 
            this.dateTimePicker7.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker7.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker7.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker7.Location = new System.Drawing.Point(214, 189);
            this.dateTimePicker7.Name = "dateTimePicker7";
            this.dateTimePicker7.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker7.TabIndex = 23;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(64, 189);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(109, 18);
            this.label64.TabIndex = 22;
            this.label64.Text = "Ngày lập phiếu:";
            // 
            // textBox26
            // 
            this.textBox26.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox26.Location = new System.Drawing.Point(214, 134);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(200, 26);
            this.textBox26.TabIndex = 21;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(64, 135);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(84, 18);
            this.label65.TabIndex = 20;
            this.label65.Text = "Mã độc giả:";
            // 
            // textBox27
            // 
            this.textBox27.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox27.Location = new System.Drawing.Point(214, 80);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(200, 26);
            this.textBox27.TabIndex = 19;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(64, 80);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(74, 18);
            this.label66.TabIndex = 18;
            this.label66.Text = "Mã phiếu:";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.button49);
            this.tabPage12.Controls.Add(this.button50);
            this.tabPage12.Controls.Add(this.button51);
            this.tabPage12.Controls.Add(this.button52);
            this.tabPage12.Controls.Add(this.button53);
            this.tabPage12.Controls.Add(this.button54);
            this.tabPage12.Controls.Add(this.button55);
            this.tabPage12.Controls.Add(this.dataGridView5);
            this.tabPage12.Controls.Add(this.button56);
            this.tabPage12.Controls.Add(this.radioButton1);
            this.tabPage12.Controls.Add(this.radioButton13);
            this.tabPage12.Controls.Add(this.textBox28);
            this.tabPage12.Controls.Add(this.label67);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(855, 404);
            this.tabPage12.TabIndex = 1;
            this.tabPage12.Text = "Tìm Kiếm Phiếu Mượn";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // button49
            // 
            this.button49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button49.ForeColor = System.Drawing.Color.White;
            this.button49.Location = new System.Drawing.Point(75, 339);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(138, 43);
            this.button49.TabIndex = 55;
            this.button49.Text = "Chỉnh Sửa";
            this.button49.UseVisualStyleBackColor = false;
            // 
            // button50
            // 
            this.button50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button50.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button50.Location = new System.Drawing.Point(672, 341);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(94, 44);
            this.button50.TabIndex = 54;
            this.button50.Text = "Hủy";
            this.button50.UseVisualStyleBackColor = false;
            // 
            // button51
            // 
            this.button51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button51.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button51.ForeColor = System.Drawing.Color.White;
            this.button51.Location = new System.Drawing.Point(315, 341);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(138, 43);
            this.button51.TabIndex = 53;
            this.button51.Text = "Lưu";
            this.button51.UseVisualStyleBackColor = false;
            // 
            // button52
            // 
            this.button52.BackColor = System.Drawing.Color.Purple;
            this.button52.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button52.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button52.Location = new System.Drawing.Point(672, 37);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(116, 33);
            this.button52.TabIndex = 52;
            this.button52.Text = "Xem Tất Cả";
            this.button52.UseVisualStyleBackColor = false;
            // 
            // button53
            // 
            this.button53.BackColor = System.Drawing.Color.Green;
            this.button53.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button53.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button53.Location = new System.Drawing.Point(493, 341);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(129, 41);
            this.button53.TabIndex = 50;
            this.button53.Text = "Lập Phiếu Trả";
            this.button53.UseVisualStyleBackColor = false;
            // 
            // button54
            // 
            this.button54.BackColor = System.Drawing.Color.Green;
            this.button54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button54.ForeColor = System.Drawing.Color.White;
            this.button54.Location = new System.Drawing.Point(167, 341);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(117, 41);
            this.button54.TabIndex = 49;
            this.button54.Text = "Xóa";
            this.button54.UseVisualStyleBackColor = false;
            // 
            // button55
            // 
            this.button55.BackColor = System.Drawing.Color.Green;
            this.button55.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button55.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button55.Location = new System.Drawing.Point(17, 341);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(117, 41);
            this.button55.TabIndex = 48;
            this.button55.Text = "Xem Chi Tiết";
            this.button55.UseVisualStyleBackColor = false;
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(7, 118);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(840, 199);
            this.dataGridView5.TabIndex = 47;
            // 
            // button56
            // 
            this.button56.BackColor = System.Drawing.Color.Purple;
            this.button56.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.button56.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button56.Location = new System.Drawing.Point(535, 35);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(123, 33);
            this.button56.TabIndex = 46;
            this.button56.Text = "Tìm Kiếm";
            this.button56.UseVisualStyleBackColor = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(271, 59);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(100, 17);
            this.radioButton1.TabIndex = 43;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Mã Phiếu Mượn";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Location = new System.Drawing.Point(271, 23);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(82, 17);
            this.radioButton13.TabIndex = 42;
            this.radioButton13.TabStop = true;
            this.radioButton13.Text = "Mã Độc Giả";
            this.radioButton13.UseVisualStyleBackColor = true;
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(399, 44);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(122, 20);
            this.textBox28.TabIndex = 41;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label67.Location = new System.Drawing.Point(77, 37);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(168, 25);
            this.label67.TabIndex = 40;
            this.label67.Text = "Tìm Kiếm Theo :";
            // 
            // frmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1128, 556);
            this.ControlBox = false;
            this.Controls.Add(this.pnThongKe);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panelDocGia);
            this.Controls.Add(this.panelQLNhanVien);
            this.Controls.Add(this.panelQLSach);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbTittle);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmHome";
            this.Load += new System.EventHandler(this.frmHome_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelQLSach.ResumeLayout(false);
            this.tbcQuanLiSach.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearchTaiLieu)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tbcDocGia.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDGSearch)).EndInit();
            this.tabThemdg.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panelDocGia.ResumeLayout(false);
            this.panelQLNhanVien.ResumeLayout(false);
            this.tbcQuanLiNV.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNhanVien)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnThongKe.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.pnLapPhieumuon.ResumeLayout(false);
            this.pnLapPhieumuon.PerformLayout();
            this.pnxemphieumuon.ResumeLayout(false);
            this.pnNhapthongtintimkiemphieumuon.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvxemphieumuon)).EndInit();
            this.tabPage9.ResumeLayout(false);
            this.pnlapphieutra.ResumeLayout(false);
            this.pnlapphieutra.PerformLayout();
            this.pnXemPhieuTra.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvXemPhieuTra)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.pnLapPhieuNhacNho.ResumeLayout(false);
            this.pnLapPhieuNhacNho.PerformLayout();
            this.pnXemPhieuNhacNho.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvXemPhieuNhacNho)).EndInit();
            this.tabPage11.ResumeLayout(false);
            this.pnLapPhieuPhat.ResumeLayout(false);
            this.pnLapPhieuPhat.PerformLayout();
            this.pnXemPhieuPhat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvXemPhieuPhat)).EndInit();
            this.panelTraCuu.ResumeLayout(false);
            this.panelTraCuu.PerformLayout();
            this.tabPhieuPhat.ResumeLayout(false);
            this.tabPhieuPhat.PerformLayout();
            this.tabPhieuNhacNho.ResumeLayout(false);
            this.tabPhieuNhacNho.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tabPhieuMuon.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabThemPhieuMuon.ResumeLayout(false);
            this.tabThemPhieuMuon.PerformLayout();
            this.tabTimKiemPhieuMuon.ResumeLayout(false);
            this.tabTimKiemPhieuMuon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPhieuTra.ResumeLayout(false);
            this.tabPhieuTra.PerformLayout();
            this.tabControl3.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbTittle;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label userName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelQLSach;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnThongKe;
        private System.Windows.Forms.Button btnDangNhap;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelQLNhanVien;
        private System.Windows.Forms.Panel panelDocGia;
        private System.Windows.Forms.TabControl tbcQuanLiNV;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.Button btnDocGia;
        private System.Windows.Forms.DataGridView dgvNhanVien;
        private System.Windows.Forms.Button btnNhanVien;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox txtMatKhauNVCapNhat;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RadioButton rdAdminCapNhat;
        private System.Windows.Forms.RadioButton rdThuThuCapNhat;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnCapNhatNhanVien;
        private System.Windows.Forms.TextBox txtHoTenNVCapNhat;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtTenDangNhapNVCapNhat;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCaTrucNVCapNhat;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtMaNVCapNhap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button btnXoaNhanVien;
        private System.Windows.Forms.TextBox txtMaNVXoa;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabControl tbcDocGia;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.Button btnXemAllDocGia;
        private System.Windows.Forms.Button btnLapPhieuCanhCao;
        private System.Windows.Forms.Button btnLapPhieuTra;
        private System.Windows.Forms.Button btnLapPhieMuon;
        private System.Windows.Forms.Button btnXoaDocGia;
        private System.Windows.Forms.Button btnXemChiTiet;
        private System.Windows.Forms.DataGridView dgvDGSearch;
        private System.Windows.Forms.Button btnSearchDocGia;
        private System.Windows.Forms.Label lblHoTenSearch;
        private System.Windows.Forms.Label lblDinhDanhSearch;
        private System.Windows.Forms.Label lblMaDGSearch;
        private System.Windows.Forms.RadioButton rdHoTenSearch;
        private System.Windows.Forms.RadioButton rdMaDinhDanhSearch;
        private System.Windows.Forms.RadioButton rdMaDGSearch;
        private System.Windows.Forms.TextBox txtSearchDG;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabPage tabThemdg;
        private System.Windows.Forms.Button btnDangKyDocGia;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtMaDG;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblMSCBDangKy;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblMSSVDangKy;
        private System.Windows.Forms.RadioButton rdKhac;
        private System.Windows.Forms.RadioButton rdCanBo;
        private System.Windows.Forms.RadioButton rdSinhVien;
        private System.Windows.Forms.TextBox txtCMNDDG;
        private System.Windows.Forms.TextBox txtMSCBDG;
        private System.Windows.Forms.TextBox txtMSSVDG;
        private System.Windows.Forms.TextBox txtEmailDG;
        private System.Windows.Forms.TextBox txtSDTDG;
        private System.Windows.Forms.TextBox txtDiaChiDG;
        private System.Windows.Forms.TextBox txtNgaySinhDG;
        private System.Windows.Forms.TextBox txtTenDG;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbUserName;
        private System.Windows.Forms.Button btnChinhSua;
        private System.Windows.Forms.ComboBox cbxDinhDanh;
        private System.Windows.Forms.TabControl tbcQuanLiSach;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnYeuCauTL;
        private System.Windows.Forms.Button btnXemTheoLoai;
        private System.Windows.Forms.ComboBox cbxLoaiTaiLieu;
        private System.Windows.Forms.Button btnChinhSuaTL;
        private System.Windows.Forms.Button btnHuyTL;
        private System.Windows.Forms.Button btnLuuTL;
        private System.Windows.Forms.Button btnXemAllTaiLieu;
        private System.Windows.Forms.Button btnLapPhieuMuonTL;
        private System.Windows.Forms.Button btnXoaTL;
        private System.Windows.Forms.Button btnXemChiTietTL;
        private System.Windows.Forms.DataGridView dgvSearchTaiLieu;
        private System.Windows.Forms.Button btnSearchTaiLieu;
        private System.Windows.Forms.Label lblTenTaiLieu;
        private System.Windows.Forms.Label lblMaTaiLieu;
        private System.Windows.Forms.RadioButton rdTimTLNangCao;
        private System.Windows.Forms.RadioButton rdTimTLCoBan;
        private System.Windows.Forms.TextBox txtSearchTaiLieu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnThemTaiLieu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMaTL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSoLuongTL;
        private System.Windows.Forms.TextBox txtLoaiTL;
        private System.Windows.Forms.TextBox txtHienTrangTL;
        private System.Windows.Forms.TextBox txtTenTL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel pnThongKe;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.Button btnXmuon;
        private System.Windows.Forms.Button btnSmuon;
        private System.Windows.Forms.Button btnCNmuon;
        private System.Windows.Forms.Button btnXPmuon;
        private System.Windows.Forms.Button btnLPmuon;
        private System.Windows.Forms.Panel pnxemphieumuon;
        private System.Windows.Forms.DataGridView dgvxemphieumuon;
        private System.Windows.Forms.Button btnXoaPhieuTra;
        private System.Windows.Forms.Button btnSuaPhieuTra;
        private System.Windows.Forms.Button btnCapNhatPhieuTra;
        private System.Windows.Forms.Button btnXemPhieuTra;
        private System.Windows.Forms.Button btnLpTra;
        private System.Windows.Forms.Button btnXoaPhieuNhacNho;
        private System.Windows.Forms.Button btnSuaPhieuNhacNho;
        private System.Windows.Forms.Button btnCapNhatPhieuNhacNho;
        private System.Windows.Forms.Button btnXemPNhacNho;
        private System.Windows.Forms.Button btnLPNhacNho;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button btnXemPhieuPhat;
        private System.Windows.Forms.Button btnLapPhieuPhat;
        private System.Windows.Forms.Panel pnLapPhieumuon;
        private System.Windows.Forms.DateTimePicker dtpNgayLapphieumuon;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtMadocgiamuon;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtMaphieumuon;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel pnlapphieutra;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel pnLapPhieuNhacNho;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel pnLapPhieuPhat;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel pnXemPhieuTra;
        private System.Windows.Forms.DataGridView dgvXemPhieuTra;
        private System.Windows.Forms.Panel pnXemPhieuNhacNho;
        private System.Windows.Forms.DataGridView dgvXemPhieuNhacNho;
        private System.Windows.Forms.Panel pnXemPhieuPhat;
        private System.Windows.Forms.DataGridView dgvXemPhieuPhat;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnthu;
        private System.Windows.Forms.Button btnLuuPhieuMuon;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.DateTimePicker dtphantraphieumuon;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtMatailieumuon;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtSTTmuon;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel pnNhapthongtintimkiemphieumuon;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Panel panelTraCuu;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lbTimKiemSach;
        private System.Windows.Forms.Label lbTimkiemDG;
        private System.Windows.Forms.TabPage tabPhieuMuon;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabThemPhieuMuon;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TabPage tabTimKiemPhieuMuon;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TabPage tabPhieuTra;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DateTimePicker dateTimePicker7;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TabPage tabPhieuPhat;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TabPage tabPhieuNhacNho;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label53;
    }
}